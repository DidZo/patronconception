#+SETUPFILE: /home/hgrall/Public/siteWeb/src/css/config.orgcss
  
#+HTML_LINK_UP: comptesRendus.html

#+HTML_MATHJAX: align:"center" mathml:t 

#+OPTIONS: H:3 num:t
#+INFOJS_OPT: view:info tdepth:3 sdepth:3 toc:t
 
#+DESCRIPTION: course summary for modularity and typing
#+KEYWORDS: IMT-Atlantique, course, modularity typing

#+TITLE: Comptes rendus

#+DATE: latest: {{{time(%Y-%m-%d)}}}

#+INDEX: course!MAPD (fr-2021-CR) - compte rendus

* Session 1 - 2 octobre 2020

** Formation des binômes et groupes

- (Adrien JALLAIS - Samir KAMAR) + (Rémi BARDON - Victor Leiglat)

- (Théo MARTEL - Martin KEYLING) - (Cynthia ANDRIAMPARIVONY - Vincent ESCOFFIER )

- Geraud SIMON - Alexis GROS | Florian DUSSABLE - Simon SASSI 

- (Guillaume GRIMAULT - Tanguy MOSSION ) + (Lucas ROURET - Romain ORVOËN)

- [ Julien RAQUOIS - Clément NICOLAS |❤| Loïs GIGAUD - Théo LETOUZÉ ]
  
- (Noé MORVILLERS - Louis MUZELLEC) + (Sylvain DOUSSET - Maël LHOUTELLIER)

- (Victoire LENGLAT - Jérémy POUZARGUE) + (Nicolas KIRCHHOFFER - Romain BARO)   

- (Yan IMENSAR, Cantin CALOONE) + (Léo BEAUJEAN, Raphaël PAINTER)

   
** Quiz 1 (prise de notes des élèves, amendée)

*** Introduction

**** Décrire l'évolution des langages de programmation.  

Assembleur(1950) -> C (1970) -> C++(1980) -> Java(1995) -> Js(1995) -> Ts(2012)

Le premier langage de programmation était l’assembleur, très proche du
binaire dans lequel il se compilait. Le langage offrait peu de typage
et de structure, rendant la réutilisation du code très difficile.

Les langages de programmation ont évolué pour permettre plus de
modularité : des principes comme ceux de la programmation orientée objet
permettent de mieux factoriser, réutiliser et structurer le code.
Le typage joue un rôle important : en effet, un type
définit l'interface du module, en faisant abstraction de la structure
interne du module.

Grâce au typage, le compilateur peut commencer par faire une analyse
syntaxique avant de transformer le code en binaire pour vérifier la
cohérence des types de données utilisés : le compilateur calcule sur les
types et non sur les valeurs ; c'est l'analogue de l'analyse
dimensionnelle en physique. Le typage permet d’éviter certaines erreurs
dès la compilation, plus généralement de faciliter l'édition de code,
mais aussi d'optimiser le code en supprimant la nécessité de contrôler
les types à l'exécution.

**** Méthode pour définir un type de données

***** Donner les deux points de vue pour définir un type.

Point de vue externe : comment utiliser des instances (éléments) de ce type ?

Point de vue interne : construction des instances (éléments) du type

Exemple : différence entre utilisation / construction d’une automobile.

Il est important de prendre en compte ces deux points de vue lors de la
définition d'un type..


***** Définir les différentes couches d'une classe d'implémentation.

Attributs / Constructeurs / Accesseurs / Fabrique / Services - Voir schéma dans
le cours.

Attributs : déterminent la structure de l'état d'un objet (plus
simplement ils définissent l’état d’un objet), suivant le mode de
construction choisi.

Remarque : On a bien souvent plusieurs modes de construction. Voir les rationnels.

Autre noms : variables d’instances, “fields” en anglais, propriétés en
Javascript

Constructeurs : initialisent directement les attributs suivant le mode
de construction choisi.  -> Initialise l’état de l’objet.

Fabriques ("factory" en anglais) : appellent les constructeurs
directement si elles correspondent au mode choisi, ou indirectement
sinon, via les fabriques et les accesseurs du mode choisi. N’a pas le
même statut qu’un constructeur: une fabrique est une méthode d’un objet,
utilisée pour fabriquer les objets de même nature. On peut utiliser une
fabrique sans connaître le constructeur. Intérêt : Avoir du code qui ne
dépend plus du tout de la classe d’implémentation. C'est un patron de
conception. Un constructeur n’est pas une méthode d’une classe : il
n'est pas appelé en utilisant un objet cible mais avec un opérateur
particulier, *new*.

Accesseurs manipulent les attributs en lecture ou en écriture
directement s'ils correspondent au mode de construction choisi, ou
indirectement sinon, via les accesseurs et les fabriques du mode de
construction choisi.  Aussi appelé “getters / setters”.

Services : accèdent aux attributs indirectement via les accesseurs et
construisant des objets indirectement via les fabriques. Ils peuvent
être ainsi implémentés indépendamment de la classe d'implémentation. 
 
Conclusion : cette structure en couches permet de factoriser au mieux le
code. 
  
*** Rationnels

**** Donner deux définitions informatiques des rationnels.

  - une fraction : numérateur / dénominateur. Par exemple : deux *int*.s
    (java) ou deux *number*.s (javascript).
  - un quotient : un réel flottant ou double (*float* ou *double* en
    java, *number* en javascript).

ex: 5/3 ou 1.666667

**** Comment utiliser un rationnel ?  

Opérations sur les rationnels :
· addition 
· soustraction 
· convertir en décimal
· convertir en fraction
· multiplication

On dispose d’un élément symétrique, un inverse ou un opposé, un élément neutre.
Bref ce type forme un “corps”, structure mathématique. 
C’est une structure mathématique commune, et beaucoup d’algorithmes
fonctionneront sur un corps quelconque.

Autre exemple de structure : un monoïde. Les chaînes de caractères
("string") forment un monoïde. Il existe des algorithmes qui
fonctionnent sur un monoïde quelconque. 

Il serait donc intéressant de pouvoir exprimer le fait que le type des
rationnels a la propriété d'être un corps.   
  
** Revue de code / rationnels

Travail par équipe de quatre (deux binômes)
- tous les groupes vus sauf
  - (Yan IMENSAR, Cantin CALOONE) + (Léo BEAUJEAN, Raphaël PAINTER)
  - (Simon Géraud, Gros Alexis).

Le but de cet exercice est de définir un type de données pour les
rationnels. Deux implémentations sont envisagées, par des fractions et
par des quotients. De plus, on aimerait profiter du fait que l'ensemble
des rationnels forme une structure algébrique particulière, un
corps. Pourrait-on utiliser les rationnels dans un algorithme conçu pour
les corps ? 

*** Préalable : comment exécuter un programme en Typescript ?

Exécution d'un script (une unité de compilation, soit un fichier,
constitué d'instructions)
- par compilation : traduction en javascript avec le compilateur *tsc*,
  puis exécution avec un interpréteur de javascript (commande *node*)
  - possibilité de configurer la compilation par un fichier
    *tsconfig.json* (voir le dépôt)
    - /dev> tsc -p src/ // compilation de l'ensemble des fichiers ts
                        //   dans src
    - /dev> node ./build/X/script.js  // exécution des scripts js dans build
                                      //   (même structure que src)
- par interprétation : utilisation de l'interpréteur *ts-node*



Le script peut importer des définitions réalisées dans d'autres unités
de compilation, avec la directive *import*.

*** Convention de nommage

Il est utile et important d'établir une convention de nommage avant de
commencer à coder. Il existe aussi des normes pour l'écriture. Cette
convention doit toujours être décrite dans un fichier (comme le
*readme*).  

Normes pour l'écriture
- Celles de java :
  https://www.oracle.com/java/technologies/javase/codeconventions-namingconventions.html
- Celles en Javascript ou Typescript peuvent être définies dans un
  fichier de configuration en utilisant un [[https://en.wikipedia.org/wiki/Lint_(software)][linter]] (analyseur) : voir
  https://github.com/typescript-eslint/typescript-eslint.

Nommage
- Chosir une langue et s'y tenir.
  - Le code que je fournirai sera toujours écrit en français. Ainsi, il
    est facile de le distinguer du code fourni par des bibliothèques ou
    des constructions du langage.
- Définir des règles pour les différentes entités. Voici par exemple les
  règles que je suis habituellement.
  - Interface : nom au singulier, éventuellement qualifié, permettant de
    bien identifier le type ou la propriété - Exemple : *Liste*    
  - Classe : reprise du nom de l'interface principale implémentée, et
    qualification pour décrire brièvement l'implémentation - Exemple :
    *ListeChainee*.
  - Méthode ou fonction : mot éventuellement qualifié décrivant le
    résultat; s'il n'y a pas d'effet, un nom, s'il y a un effet, un
    verbe - Exemple : *miroir() : Liste* ou *inverser() : void* pour la
    méthode inversant la liste, soit en créant une nouvelle liste, soit
    en modifiant la liste cible (pointée par *this*).
  - Fabrique : *creer(...)*
  - Attribut : nom décrivant le rôle dans l'état de l'objet  

*** Méthode pour définir un type

Il est important de distinguer l'interface des deux classes
d'implémentation, correspondant aux deux modes de construction des
rationnels, par fraction ou par quotient. Dans nos discussions, j'ai
évalué la distance de votre proposition avec la solution ci-dessous.

Contenu de l'interface
- services algébriques (ceux d'un corps)
- accesseurs correspondant au mode de construction par fraction
- accesseurs correspondant au mode de construction par quotient    
- fabrique à partir d'une fraction (deux *number*.s)
- fabrique à partir d'un quotient (un *number*)

Contenu de la classe d'implémentation par fraction
- attributs : le numérateur et le dénominateur (deux *number*.s entiers
- un constructeur initialisant le numérateur et le dénominateur
- accesseurs correspondant au mode de construction par fraction :
  implémentation par accès direct aux attributs
- accesseurs correspondant au mode de construction par quotient :
  implémentation par calcul (une division)
- fabrique à partir d'une fraction (deux *number*.s) : appel du
  constructeur
- fabrique à partir d'un quotient (un *number*) : appel du constructeur
  après un calcul (problème difficile, résolu par les
  [[https://fr.wikipedia.org/wiki/Fraction_continue_et_approximation_diophantienne][fractions continues]])
- services : implémentation en utilisant les accesseurs et les fabriques 

Contenu de la classe d'implémentation par quotient
- attributs : le quotient (un *number* flottant)
- un constructeur initialisant le quotient
- accesseurs correspondant au mode de construction par fraction :
  implémentation par calcul (toujours le même problème difficile)
- accesseurs correspondant au mode de construction par quotient :
  implémentation par accès direct à l'attribut
- fabrique à partir d'une fraction (deux *number*.s) : appel du
  constructeur après un calcul simple (une division)
- fabrique à partir d'un quotient (un *number*) : appel du constructeur
- services : implémentation en utilisant les accesseurs et les fabriques 

*** Quelques techniques utiles

**** Etat
    
En Typescript, le constructeur est unique et se nomme *constructor*. Si
on a besoin d'autres constructeurs, on définit des fonctions adjointes
en dehors de la classe, des fabriques appelant le constructeur.

Le constructeur possède une forme abrégée permettant de déclarer
automatiquement les attributs et de les initialiser à partir de ses
paramètres. Il est recommandé d'utiliser cette forme, très concise.

#+begin_src js
  constructor(private attribut : type) {}
#+end_src

Voir la
[[https://www.typescriptlang.org/docs/handbook/classes.html#parameter-properties][documentation]] consacrée à la déclaration d'attributs par les
paramètres du constructeur.

**** Types construits

Javascript comme Typescript incluent le langage Json pour décrire des
données. Plutôt qu'utiliser plusieurs attributs ou plusieurs paramètres,
ou encore plusieurs accesseurs, on peut utiliser un attribut, un
paramètre et un accesseur, à condition d'utiliser un type composé.

Exemple : une fraction peut être vue comme un couple de type *[number,
number]*, le produit cartésien de *number* par lui-même. Voir dans la
documentation les n-uplets, appelés [[https://www.typescriptlang.org/docs/handbook/basic-types.html#tuple][tuples]] en anglais. L'utilisation de
ce type composé permet d'avoir un seul attribut, un seul paramètre pour
le constructeur à partir d'une fraction, et un seul accesseur en
lecture pour la fraction. Un autre avantage est de permettre de calculer
en une seule fois la fraction associée à un quotient : c'est utile
puisque la conversion demande un calcul qui peut être coûteux.   
     
*** Données mutables, données immutables

Un objet est dit *mutable* si son état peut changer après son
initialisation, *immutable* sinon. En cas de mutabilité, les accesseurs
contiennent en plus des accesseurs en lecture (les "getters") des
accesseurs en écriture (les "setters"). Aussi, les calculs, comme la somme ou
le produit peuvent s'effectuer de deux manières :
- d'une manière immutable, en créant un nouvel objet sans modifier l'objet cible,
- d'une manière mutable, en modifiant en place l'objet cible.  

Lors de la revue de code, j'ai observé les trois situations suivantes :
- (A) des accesseurs en lecture, des calculs immutables,
- (B) des accesseurs en en lecture et en écriture, des calculs immutables,
- (C) des accesseurs en en lecture et en écriture, des calculs mutables.

Le choix (B) manque de cohérence, puisque les accesseurs en écriture ne
servent pas aux calculs. 

Le choix entre la mutabilité et l'immutabilité doit être argumenté et
clairement annoncé : c'est qu'en effet il a de nombreuses conséquences
sur deux questions fondamentales, la *correction* (le fait d'être
correct) des programmes et leur complexité (le fait d'être
efficace). Une leçon est consacrée à ce sujet.

*** L'expression d'une propriété

Comment exprimer que le type des rationnels, exprimé par une interface,
a la propriété d'être un corps ? La solution est d'utiliser une
interface générique, c'est-à-dire paramétrée par un type. Une interface
ressemble alors à un prédicat en logique :
- une interface non paramétrée est comme un prédicat non paramétré,
  autrement dit une proposition, déclarant les méthodes présentes dans
  un type,
- une interface paramétrée est comme un prédicat paramétré, exprimant
  les propriétés du paramètre,

Pour exprimer que l'interface *Rationnel* a la propriété d'être un
corps, on introduit une interface générique *Corps* et on déclare que
*Rationnel* hérite de *Corps<Rationnel>*.
#+begin_src js
  interface Corps<T> { ... } // T : paramètre

  // Rationnel a la propriété d'être un corps.
  interface Rationnel extends Corps<Rationnel> { ... }
#+end_src

Une leçon est consacrée à ce sujet.



    
    
