"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.heritageType = exports.heritage = exports.heriterInterfacesConcretes = void 0;
// Fonction permettant d'hériter de tous les champs.
function heriterInterfacesConcretes(descendant, ascendants) {
    ascendants.forEach(ascendant => {
        Object.getOwnPropertyNames(ascendant.prototype).forEach(nom => {
            descendant.prototype[nom] = ascendant.prototype[nom];
        });
    });
}
exports.heriterInterfacesConcretes = heriterInterfacesConcretes;
// Fonctions permettant l'héritage d'un champ qu'on doit nommer.
// - usage : this.f = heritage(f, Classe);
// - sans contrôle de typage (à la js)
function heritage(nomChamp, classeParente) {
    return classeParente.prototype[nomChamp];
}
exports.heritage = heritage;
// - usage : this.f = heritageType<SousType, SurType>('f', SurType)
//   (SousType < SurType, 'f' champ de SurType)
// - avec contrôle de typage (à la ts)
function heritageType(nomChamp, classeParente) {
    return classeParente.prototype[nomChamp];
}
exports.heritageType = heritageType;
//# sourceMappingURL=heritage.js.map