"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEF = exports.tableauCommeListe = exports.listeCommeTableau = exports.tailleRecursive = exports.concatenation = exports.reste = exports.tete = exports.VIDE = exports.visiteFonctionRecursivePrimitive = exports.visiteFonctionIterative = exports.filtrage = exports.estVide = void 0;
function estVide(l) {
    return ("vide" in l);
}
exports.estVide = estVide;
function filtrage(l, casVide, casCons) {
    if (estVide(l)) {
        return casVide();
    }
    else {
        return casCons(l[0], l[1]);
    }
}
exports.filtrage = filtrage;
function visiteFonctionIterative(l, casVide, casCons) {
    if (estVide(l)) {
        return casVide();
    }
    else {
        return casCons(l[0], visiteFonctionIterative(l[1], casVide, casCons));
    }
}
exports.visiteFonctionIterative = visiteFonctionIterative;
function visiteFonctionRecursivePrimitive(l, casVide, casCons) {
    if (estVide(l)) {
        return casVide();
    }
    else {
        return casCons(l[0], l[1], visiteFonctionRecursivePrimitive(l[1], casVide, casCons));
    }
}
exports.visiteFonctionRecursivePrimitive = visiteFonctionRecursivePrimitive;
const VIDE_singleton = { vide: "un" };
function VIDE() {
    return VIDE_singleton;
}
exports.VIDE = VIDE;
function tete(l) {
    if (estVide(l))
        throw new Error('tête indéfinie : liste vide');
    return l[0];
}
exports.tete = tete;
function reste(l) {
    if (estVide(l))
        throw new Error('tête indéfinie : liste vide');
    return l[1];
}
exports.reste = reste;
function concatenation(l, k) {
    let m = VIDE();
    while (!estVide(l)) {
        m = [l[0], m];
        l = l[1];
    }
    while (!estVide(m)) {
        k = [m[0], k];
        m = m[1];
    }
    return k;
}
exports.concatenation = concatenation;
function tailleRecursive(l) {
    return filtrage(l, () => 0, (t, l) => 1 + tailleRecursive(l));
}
exports.tailleRecursive = tailleRecursive;
function listeCommeTableau(l) {
    let res = [];
    while (!estVide(l)) {
        res.push(l[0]);
        l = l[1];
    }
    return res;
}
exports.listeCommeTableau = listeCommeTableau;
function tableauCommeListe(tab) {
    let l = VIDE();
    for (let i = tab.length - 1; i >= 0; i--) {
        l = [tab[i], l];
    }
    return l;
}
exports.tableauCommeListe = tableauCommeListe;
function DEF(...args) {
    return tableauCommeListe(args);
}
exports.DEF = DEF;
//# sourceMappingURL=listes.js.map