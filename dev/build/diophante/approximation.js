"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.approximation = void 0;
const MODE_VERBEUX = true;
const PRECISION = 1e-15; // Précision relative
const INVERSE_MAX_INT = 1.1 / Number.MAX_SAFE_INTEGER;
/*
 * Précondition : Abs(e.r - e.p) > INVERSE_MAX_INT
 */
function suivant(e) {
    let p = Math.floor(e.r);
    return {
        r: 1.0 / (e.r - p),
        // équations de récurrence
        h2: e.h2 * p + e.h1,
        h1: e.h2,
        k2: e.k2 * p + e.k1,
        k1: e.k2
    };
}
function numerateur(e) {
    return suivant(e).h2;
}
function denominateur(e) {
    return suivant(e).k2;
}
function approximation(r) {
    const abs = Math.abs(r);
    const signe = (r >= 0);
    let courant = {
        r: abs,
        h2: 1,
        h1: 0,
        k2: 0,
        k1: 1
    };
    let n = 0;
    while (estRelativementTropLoin(abs, numerateur(courant), denominateur(courant))
        && partieDecimaleAssezGrande(courant.r)) {
        courant = suivant(courant);
        n++;
    }
    if (MODE_VERBEUX)
        console.log("- Profondeur : " + n);
    let h = numerateur(courant);
    let k = denominateur(courant);
    h = signe ? h : -h;
    return [h, k];
}
exports.approximation = approximation;
function partieDecimaleAssezGrande(r) {
    let p = Math.floor(r);
    let condition = (r - p) > INVERSE_MAX_INT;
    if (MODE_VERBEUX && !condition) {
        console.log("- Dépassement : " + (r - p));
    }
    return condition;
}
function estRelativementTropLoin(r, h, k) {
    let condition = Math.abs(r - h / k) > r * PRECISION;
    if (MODE_VERBEUX && !condition) {
        console.log("- Bonne approximation : "
            + Math.abs(r - h / k) + " / " + r);
    }
    return condition;
}
//# sourceMappingURL=approximation.js.map