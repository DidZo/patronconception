"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const approximation_1 = require("./approximation");
let r = Math.PI;
console.log("PI : " + r);
let a = approximation_1.approximation(r);
console.log(a[0] + " / " + a[1]);
console.log(a[0] / a[1]);
// Voir https://oeis.org/A327360.
r = 34.0 / 6.0;
console.log("34.0 / 6.0 : " + r);
a = approximation_1.approximation(r);
console.log(a[0] + " / " + a[1]);
console.log(a[0] / a[1]);
r = 517.0 / 23.0;
console.log("517.0 / 23.0 : " + r);
a = approximation_1.approximation(r);
console.log(a[0] + " / " + a[1]);
console.log(a[0] / a[1]);
r = Math.E;
console.log("e : " + r);
a = approximation_1.approximation(r);
console.log(a[0] + " / " + a[1]);
console.log(a[0] / a[1]);
// Voir http://oeis.org/A007676
r = 333 / 1000;
console.log("333 / 1000 : " + r);
a = approximation_1.approximation(r);
console.log(a[0] + " / " + a[1]);
console.log(a[0] / a[1]);
//# sourceMappingURL=testApproximation.js.map