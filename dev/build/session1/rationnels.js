"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FABRIQUE_RationnelParQuotient = exports.FABRIQUE_RationnelParFraction = void 0;
const approximation_1 = require("../diophante/approximation");
class RationnelParFraction {
    constructor(numerateur_denominateur) {
        this.numerateur_denominateur = numerateur_denominateur;
    }
    fraction() {
        return this.numerateur_denominateur;
    }
    valeur() {
        return this.numerateur_denominateur[0] / this.numerateur_denominateur[1];
    }
    creerParFraction(f) {
        return new RationnelParFraction(f);
    }
    creerParNombre(q) {
        return new RationnelParFraction(approximation_1.approximation(q));
    }
    somme(x) {
        return this.creerParFraction([
            this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
    }
    zero() {
        return this.creerParFraction([0, 1]);
    }
    produit(x) {
        return this.creerParFraction([
            this.fraction()[0] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
    }
    un() {
        return this.creerParFraction([1, 1]);
    }
    oppose() {
        return this.creerParFraction([
            -this.fraction()[0],
            this.fraction()[1]
        ]);
    }
    inverse() {
        return this.creerParFraction([
            this.fraction()[1],
            this.fraction()[0]
        ]);
    }
    estEgal(x) {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation() {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}
class RationnelParQuotient {
    constructor(quotient) {
        this.quotient = quotient;
    }
    fraction() {
        return approximation_1.approximation(this.quotient);
    }
    valeur() {
        return this.quotient;
    }
    creerParFraction(f) {
        return new RationnelParQuotient(f[0] / f[1]);
    }
    creerParNombre(q) {
        return new RationnelParQuotient(q);
    }
    somme(x) {
        return this.creerParNombre(this.valeur() + x.valeur());
    }
    zero() {
        return this.creerParNombre(0);
    }
    produit(x) {
        return this.creerParNombre(this.valeur() * x.valeur());
    }
    un() {
        return this.creerParNombre(1);
    }
    oppose() {
        return this.creerParNombre(-this.valeur());
    }
    inverse() {
        return this.creerParNombre(1 / this.valeur());
    }
    estEgal(x) {
        return this.valeur() === x.valeur();
    }
    representation() {
        return String(this.valeur());
    }
}
class RationnelParQuotientEfficace extends RationnelParQuotient {
    constructor(quotient) {
        super(quotient);
        this._fraction = [0, 0];
    }
    fraction() {
        if (this._fraction[1] == 0)
            this._fraction = super.fraction();
        return this._fraction;
    }
}
exports.FABRIQUE_RationnelParFraction = new RationnelParFraction([0, 1]);
exports.FABRIQUE_RationnelParQuotient = new RationnelParQuotient(0);
let x = new RationnelParQuotientEfficace(5 / 3);
console.log(x.fraction());
console.log(x.fraction());
let y = x;
console.log(y.fraction());
//# sourceMappingURL=rationnels.js.map