"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FABRIQUE_RationnelParQuotient = exports.FABRIQUE_RationnelParFraction = void 0;
const approximation_1 = require("../diophante/approximation");
class RationnelParFraction {
    constructor(numerateur_denominateur) {
        this.numerateur_denominateur = numerateur_denominateur;
    }
    changerFraction(f) {
        this.numerateur_denominateur = f;
    }
    changerValeur(q) {
        this.changerFraction(approximation_1.approximation(q));
    }
    fraction() {
        return this.numerateur_denominateur;
    }
    valeur() {
        return this.numerateur_denominateur[0] / this.numerateur_denominateur[1];
    }
    creerParFraction(f) {
        return new RationnelParFraction(f);
    }
    creerParNombre(q) {
        return new RationnelParFraction(approximation_1.approximation(q));
    }
    somme(x) {
        this.changerFraction([
            this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
        return this;
    }
    zero() {
        this.changerFraction([0, 1]);
        return this;
    }
    produit(x) {
        this.changerFraction([
            this.fraction()[0] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
        return this;
    }
    un() {
        this.changerFraction([1, 1]);
        return this;
    }
    oppose() {
        this.changerFraction([
            -this.fraction()[0],
            this.fraction()[1]
        ]);
        return this;
    }
    inverse() {
        this.changerFraction([
            this.fraction()[1],
            this.fraction()[0]
        ]);
        return this;
    }
    estEgal(x) {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation() {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}
class RationnelParQuotient {
    constructor(quotient) {
        this.quotient = quotient;
    }
    changerFraction(f) {
        this.changerValeur(f[0] / f[1]);
    }
    changerValeur(q) {
        this.quotient = q;
    }
    fraction() {
        return approximation_1.approximation(this.quotient);
    }
    valeur() {
        return this.quotient;
    }
    creerParFraction(f) {
        return new RationnelParQuotient(f[0] / f[1]);
    }
    creerParNombre(q) {
        return new RationnelParQuotient(q);
    }
    somme(x) {
        this.changerValeur(this.valeur() + x.valeur());
        return this;
    }
    zero() {
        this.changerValeur(0);
        return this;
    }
    produit(x) {
        this.changerValeur(this.valeur() * x.valeur());
        return this;
    }
    un() {
        this.changerValeur(1);
        return this;
    }
    oppose() {
        this.changerValeur(-this.valeur());
        return this;
    }
    inverse() {
        this.changerValeur(1 / this.valeur());
        return this;
    }
    estEgal(x) {
        return this.valeur() === x.valeur();
    }
    representation() {
        return String(this.valeur());
    }
}
exports.FABRIQUE_RationnelParFraction = new RationnelParFraction([0, 1]);
exports.FABRIQUE_RationnelParQuotient = new RationnelParQuotient(0);
//# sourceMappingURL=rationnelsMutables.js.map