"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rationnels_1 = require("./rationnels");
const rationnelsMutables_1 = require("./rationnelsMutables");
const r1 = rationnels_1.FABRIQUE_RationnelParFraction.creerParFraction([1, 3]);
const r2 = rationnels_1.FABRIQUE_RationnelParFraction.creerParFraction([2, 3]);
console.log("faux ? " + r1.estEgal(r1.somme(r2)));
//  Mathématiquement  : 1/3 = 1/3 + 2/3
const rm1 = rationnelsMutables_1.FABRIQUE_RationnelParFraction.creerParFraction([1, 3]);
const rm2 = rationnelsMutables_1.FABRIQUE_RationnelParFraction.creerParFraction([2, 3]);
console.log("faux ? " + rm1.estEgal(rm1.somme(rm2)));
//  Mathématiquement : 1/3 = 1/3 + 2/3
//  Informatiquement : rationnel pointé par rm1 
//              égal à rationnel pointé par rm1
//# sourceMappingURL=testEgaliteMutabilite.js.map