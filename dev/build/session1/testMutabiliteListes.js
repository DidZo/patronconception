"use strict";
// dev> ts-node -P src/tsconfig.json src/session1/X.ts 
// après installation de perf_hooks (indication donnée)
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Comparaison entre un tableua et une liste chaînée
 *
 */
const modulePerf = require("perf_hooks");
const listes_1 = require("../bibliotheque/listes");
const max = 20000;
let t1 = modulePerf.performance.now();
let sm = [];
for (let i = 0; i < max; i++) {
    sm.push('a');
}
console.log(sm[max - 1]);
let t2 = modulePerf.performance.now();
console.log("Test tableaux mutables (temps en ms) : " + (t2 - t1));
t1 = modulePerf.performance.now();
let si = listes_1.VIDE();
for (let i = 0; i < max; i++) {
    si = listes_1.concatenation(si, ['a', listes_1.VIDE()]);
}
console.log(listes_1.tete(si));
t2 = modulePerf.performance.now();
console.log("Test listes immutables (temps en ms) : " + (t2 - t1));
function fonctionPenalisante(l) {
    for (let i = 0; i < max; i++) {
        l = listes_1.concatenation(l, ['a', listes_1.VIDE()]);
    }
    return l;
}
function fonctionPlusEfficace(l) {
    let tab = listes_1.listeCommeTableau(l);
    for (let i = 0; i < max; i++) {
        tab.push('a');
    }
    return listes_1.tableauCommeListe(tab);
}
t1 = modulePerf.performance.now();
fonctionPenalisante(listes_1.VIDE());
t2 = modulePerf.performance.now();
console.log("Test fonction pénalisante (temps en ms) : " + (t2 - t1));
t1 = modulePerf.performance.now();
fonctionPlusEfficace(listes_1.VIDE());
t2 = modulePerf.performance.now();
console.log("Test fonction plus efficace (temps en ms) : " + (t2 - t1));
//# sourceMappingURL=testMutabiliteListes.js.map