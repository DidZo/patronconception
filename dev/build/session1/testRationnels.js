"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rationnels_1 = require("./rationnels");
let n = rationnels_1.FABRIQUE_RationnelParQuotient.creerParNombre(5 / 3);
console.log(n.representation());
n = rationnels_1.FABRIQUE_RationnelParQuotient.creerParFraction([5, 3]);
console.log(n.representation());
let m = rationnels_1.FABRIQUE_RationnelParFraction.creerParFraction([3, 2]);
console.log(m.representation());
m = rationnels_1.FABRIQUE_RationnelParFraction.creerParNombre(3 / 2);
console.log(m.representation());
console.log(m.somme(n).representation());
console.log(n.somme(m).representation());
console.log(m.produit(n).representation());
console.log(n.produit(m).representation());
m = rationnels_1.FABRIQUE_RationnelParFraction.creerParNombre(Math.PI);
console.log(m.representation());
//# sourceMappingURL=testRationnels.js.map