"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const heritage_1 = require("../bibliotheque/heritage");
// Classe donnant une implémentation par défaut
class Abstrait_defaut {
    f() {
        console.log("Abstrait.f");
    }
}
class ImplemConcret {
    g() {
        console.log("Concret.g");
    }
}
class Implem extends ImplemConcret {
    constructor() {
        super();
        // Agrégation du code
        this.f = heritage_1.heritageType('f', Abstrait_defaut);
    }
}
let c = new Implem();
c.f();
c.g();
let c1 = new Implem();
console.log("identité des méthodes ? " + (c.f === c1.f));
// Conclusion : partage du code !
//# sourceMappingURL=archiCouches_heritageMultiple.js.map