"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const communication_1 = require("./communication");
class CanalOutProtocole1 {
    emettre(msg) {
        communication_1.emettre(1, msg);
    }
}
class CanalOutProtocole2 {
    emettre(msg) {
        communication_1.emettre(2, msg);
    }
}
class AgentDeleguantACanal {
    // Attribut de type Canal et constructeur associé
    constructor(canal) {
        this.canal = canal;
    }
    emettre(msg) {
        this.canal.emettre(msg);
    }
}
class AgentDecoupantMessages extends AgentDeleguantACanal {
    constructor(canal) {
        super(canal);
    }
    envoyer(msg) {
        communication_1.envoyerApresDecoupage(this, msg);
    }
}
class AgentEncapsulantMessages extends AgentDeleguantACanal {
    constructor(canal) {
        super(canal);
    }
    envoyer(msg) {
        communication_1.envoyerApresEncapsulation(this, msg);
    }
}
let a = new AgentDecoupantMessages(new CanalOutProtocole1());
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentDecoupantMessages(new CanalOutProtocole2());
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessages(new CanalOutProtocole1());
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessages(new CanalOutProtocole2());
a.envoyer("nul n'est censé ignorer les principes de modularité.");
//# sourceMappingURL=agregation.js.map