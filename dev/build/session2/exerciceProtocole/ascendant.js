"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const communication_1 = require("./communication");
class CanalOutProtocole1 {
    emettre(msg) {
        communication_1.emettre(1, msg);
    }
}
class CanalOutProtocole2 {
    emettre(msg) {
        communication_1.emettre(2, msg);
    }
}
class AgentEncapsulantMessagesPourProtocole1 extends CanalOutProtocole1 {
    envoyer(msg) {
        communication_1.envoyerApresEncapsulation(this, msg);
    }
}
class AgentEncapsulantMessagesPourProtocole2 extends CanalOutProtocole2 {
    envoyer(msg) {
        communication_1.envoyerApresEncapsulation(this, msg);
    }
}
class AgentDecoupantMessagesPourProtocole1 extends CanalOutProtocole1 {
    envoyer(msg) {
        communication_1.envoyerApresDecoupage(this, msg);
    }
}
class AgentDecoupantMessagesPourProtocole2 extends CanalOutProtocole2 {
    envoyer(msg) {
        communication_1.envoyerApresDecoupage(this, msg);
    }
}
let a = new AgentDecoupantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentDecoupantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
//# sourceMappingURL=ascendant.js.map