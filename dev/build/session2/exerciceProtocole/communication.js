"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.envoyerApresDecoupage = exports.envoyerApresEncapsulation = exports.emettre = void 0;
function emettre(numero, msg) {
    console.log("protocole " + numero + " : " + msg);
}
exports.emettre = emettre;
// Encapsulation
function envoyerApresEncapsulation(canal, msg) {
    canal.emettre(msg);
}
exports.envoyerApresEncapsulation = envoyerApresEncapsulation;
// Découpage
function envoyerApresDecoupage(canal, msg) {
    const TAILLE = 5;
    const q = msg.length / TAILLE;
    const r = msg.length % TAILLE;
    for (let j = 0; j < q; j++) {
        canal.emettre(msg.substring(j * TAILLE, (j + 1) * TAILLE));
    }
    canal.emettre(msg.substring(q * TAILLE, q * TAILLE + r));
}
exports.envoyerApresDecoupage = envoyerApresDecoupage;
//# sourceMappingURL=communication.js.map