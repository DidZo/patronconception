"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const communication_1 = require("./communication");
class AgentDecoupantMessages {
    envoyer(msg) {
        communication_1.envoyerApresDecoupage(this, msg);
    }
}
class AgentEncapsulantMessages {
    envoyer(msg) {
        communication_1.envoyerApresEncapsulation(this, msg);
    }
}
class AgentDecoupantMessagesPourProtocole1 extends AgentDecoupantMessages {
    emettre(msg) {
        communication_1.emettre(1, msg);
    }
}
class AgentDecoupantMessagesPourProtocole2 extends AgentDecoupantMessages {
    emettre(msg) {
        communication_1.emettre(2, msg);
    }
}
class AgentEncapsulantMessagesPourProtocole1 extends AgentEncapsulantMessages {
    emettre(msg) {
        communication_1.emettre(1, msg);
    }
}
class AgentEncapsulantMessagesPourProtocole2 extends AgentEncapsulantMessages {
    emettre(msg) {
        communication_1.emettre(2, msg);
    }
}
let a = new AgentDecoupantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentDecoupantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
//# sourceMappingURL=descendant.js.map