"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const heritage_1 = require("../../bibliotheque/heritage");
const communication_1 = require("./communication");
class AgentDecoupantMessages {
    envoyer(msg) {
        communication_1.envoyerApresDecoupage(this, msg);
    }
}
class AgentEncapsulantMessages {
    envoyer(msg) {
        communication_1.envoyerApresEncapsulation(this, msg);
    }
}
class CanalOutProtocole1 {
    emettre(msg) {
        communication_1.emettre(1, msg);
    }
}
class CanalOutProtocole2 {
    emettre(msg) {
        communication_1.emettre(2, msg);
    }
}
class AgentDecoupantMessagesPourProtocole1 extends CanalOutProtocole1 {
    constructor() {
        super();
        this.envoyer = AgentDecoupantMessages.prototype['envoyer']; //heritageType<AgentDecoupantMessagesPourProtocole1, AgentDecoupantMessages>('envoyer', AgentDecoupantMessages);
    }
}
class AgentDecoupantMessagesPourProtocole2 extends CanalOutProtocole2 {
    constructor() {
        super();
        this.envoyer = AgentDecoupantMessages.prototype['envoyer'];
        //ou : heritageType<AgentDecoupantMessagesPourProtocole2,  
        //                  AgentDecoupantMessages>(
        //        'envoyer', AgentDecoupantMessages);
    }
}
class AgentEncapsulantMessagesPourProtocole1 extends CanalOutProtocole1 {
    constructor() {
        super();
        this.envoyer = heritage_1.heritageType('envoyer', AgentEncapsulantMessages);
    }
}
class AgentEncapsulantMessagesPourProtocole2 extends CanalOutProtocole2 {
    constructor() {
        super();
        this.envoyer = heritage_1.heritageType('envoyer', AgentEncapsulantMessages);
    }
}
let a = new AgentDecoupantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentDecoupantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
a = new AgentEncapsulantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");
//# sourceMappingURL=heritageMultiple.js.map