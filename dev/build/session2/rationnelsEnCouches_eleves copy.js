"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.nombre = exports.fraction = void 0;
const approximation_1 = require("../diophante/approximation");
/*
 * Implémentation d'une fabrique avec une classe anonyme.
 */
function fraction(frac) {
    return new class {
        valeur() {
            return frac[0] / frac[1];
        }
        creerParFraction(f) {
            return fraction(f);
        }
        creerParNombre(q) {
            return fraction(approximation_1.approximation(q));
        }
        fraction() {
            return frac;
        }
        representation() {
            return this.fraction()[0]
                + "/" + this.fraction()[1];
        }
        estEgal(x) {
            const f = this.fraction();
            const g = x.fraction();
            return f[0] * g[1] === f[1] * g[0];
        }
    };
}
exports.fraction = fraction;
;
/*
 * Implémentation d'une fabrique avec une classe anonyme.
 */
function nombre(val) {
    return new class {
        fraction() {
            return approximation_1.approximation(val);
        }
        creerParFraction(f) {
            return nombre(f[0] / f[1]);
        }
        creerParNombre(q) {
            return nombre(q);
        }
        valeur() {
            return val;
        }
        representation() {
            return String(this.valeur());
        }
        estEgal(x) {
            return this.valeur() === x.valeur();
        }
    };
}
exports.nombre = nombre;
;
/*
* 2. A FAIRE - La couche haute : agrégation avec délégation
*/
class RationnelDeleguantAEtat {
    constructor(nombreRationnel) {
        this.nombreRationnel = nombreRationnel;
    }
    fraction() {
        return this.nombreRationnel.fraction();
    }
    valeur() {
        return this.nombreRationnel.valeur();
    }
    representation() {
        return this.nombreRationnel.representation();
    }
    estEgal(x) {
        return this.nombreRationnel.estEgal(x);
    }
}
class RationnelOperationParQuotient extends RationnelDeleguantAEtat {
    constructor(nombreRationnel) {
        super(nombreRationnel);
    }
    creerParFraction(f) {
        return new RationnelOperationParQuotient(fraction(f));
    }
    creerParNombre(q) {
        return new RationnelOperationParQuotient(nombre(q));
    }
    somme(x) {
        return this.creerParNombre(this.valeur() + x.valeur());
    }
    zero() {
        return this.creerParNombre(0);
    }
    produit(x) {
        return this.creerParNombre(this.valeur() * x.valeur());
    }
    un() {
        return this.creerParNombre(1);
    }
    oppose() {
        return this.creerParNombre(-this.valeur());
    }
    inverse() {
        return this.creerParNombre(1 / this.valeur());
    }
    estEgal(x) {
        return this.valeur() === x.valeur();
    }
    representation() {
        return String(this.valeur());
    }
}
class RationnelOperationParFraction extends RationnelDeleguantAEtat {
    constructor(nombreRationnel) {
        super(nombreRationnel);
    }
    creerParFraction(f) {
        return new RationnelOperationParFraction(fraction(f));
    }
    creerParNombre(q) {
        return new RationnelOperationParFraction(nombre(q));
    }
    somme(x) {
        return this.creerParFraction([
            this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
    }
    zero() {
        return this.creerParFraction([0, 1]);
    }
    produit(x) {
        return this.creerParFraction([
            this.fraction()[0] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
    }
    un() {
        return this.creerParFraction([1, 1]);
    }
    oppose() {
        return this.creerParFraction([
            -this.fraction()[0],
            this.fraction()[1]
        ]);
    }
    inverse() {
        return this.creerParFraction([
            this.fraction()[1],
            this.fraction()[0]
        ]);
    }
    estEgal(x) {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation() {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}
class EtatRationnelConcret {
    constructor(etatRationnel) {
        this.etatRationnel = etatRationnel;
    }
    ;
    setState(etatRationnelP) {
        this.etatRationnel = etatRationnelP;
    }
    creerParFraction(f) {
        return new RationnelOperationParFraction(fraction(f));
    }
    creerParNombre(q) {
        return new RationnelOperationParFraction(nombre(q));
    }
    somme(x) {
        return this.etatRationnel.somme(x);
    }
    zero() {
        return this.etatRationnel.zero();
    }
    produit(x) {
        return this.etatRationnel.produit(x);
    }
    un() {
        return this.etatRationnel.un();
    }
    oppose() {
        return this.etatRationnel.oppose();
    }
    inverse() {
        return this.etatRationnel.inverse();
    }
    estEgal(x) {
        return this.etatRationnel.estEgal(x);
    }
    representation() {
        return this.etatRationnel.representation();
    }
}
// TODO: une strategy n'est pas un rationnel.  A ne pas utiliser avec Etat
class RationnelStrategy {
    constructor(strategyEtatRationnel, strategyOperationRationnel) {
        this.strategyEtatRationnel = strategyEtatRationnel;
        this.strategyOperationRationnel = strategyOperationRationnel;
    }
    setEtatStrategy(strategyEtatRationnelP) {
        this.strategyEtatRationnel = strategyEtatRationnelP;
    }
    setOperationStrategy(strategyOperationRationnelP) {
        this.strategyOperationRationnel = strategyOperationRationnelP;
    }
    somme(x) {
        return this.strategyOperationRationnel.somme(x); // .somme(this.x)
    }
    zero() {
        return this.strategyOperationRationnel.zero();
    }
    produit(x) {
        return this.strategyOperationRationnel.produit(x);
    }
    un() {
        return this.strategyOperationRationnel.un();
    }
    oppose() {
        return this.strategyOperationRationnel.oppose();
    }
    inverse() {
        return this.strategyOperationRationnel.inverse();
    }
    valeur() {
        return this.strategyEtatRationnel.valeur();
    }
    fraction() {
        return this.strategyEtatRationnel.fraction();
    }
    creerParFraction(f) {
        return this.strategyEtatRationnel.creerParFraction(f);
    }
    creerParNombre(q) {
        return this.strategyEtatRationnel.creerParNombre(q);
    }
    representation() {
        return this.strategyEtatRationnel.representation();
    }
    estEgal(x) {
        return this.strategyEtatRationnel.estEgal(x);
    }
}
//# sourceMappingURL=rationnelsEnCouches_eleves copy.js.map