"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.monRationnelPolymorphe = exports.calculsParFraction = exports.quotientDimorphe = exports.fractionDimorphe = exports.nombre = exports.fraction = void 0;
const approximation_1 = require("../diophante/approximation");
/*
 * Implementation d'une fabrique avec une classe anonyme.
 */
function fraction(frac) {
    return new class {
        valeur() {
            return frac[0] / frac[1];
        }
        creerParFraction(f) {
            return fraction(f);
        }
        creerParNombre(q) {
            return fraction(approximation_1.approximation(q));
        }
        fraction() {
            return frac;
        }
        representation() {
            return this.fraction()[0]
                + "/" + this.fraction()[1];
        }
        estEgal(x) {
            const f = this.fraction();
            const g = x.fraction();
            return f[0] * g[1] === f[1] * g[0];
        }
    };
}
exports.fraction = fraction;
;
/*
 * Implémentation d'une fabrique avec une classe anonyme.
 */
function nombre(val) {
    return new class {
        fraction() {
            return approximation_1.approximation(val);
        }
        creerParFraction(f) {
            return nombre(f[0] / f[1]);
        }
        creerParNombre(q) {
            return nombre(q);
        }
        valeur() {
            return val;
        }
        representation() {
            return String(this.valeur());
        }
        estEgal(x) {
            return this.valeur() === x.valeur();
        }
    };
}
exports.nombre = nombre;
;
/*
* 2. A FAIRE - La couche haute : agregation avec delegation
*/
class RationnelDeleguantAEtat {
    constructor(nombreRationnel) {
        this.nombreRationnel = nombreRationnel;
    }
    fraction() {
        return this.nombreRationnel.fraction();
    }
    valeur() {
        return this.nombreRationnel.valeur();
    }
    representation() {
        return this.nombreRationnel.representation();
    }
    estEgal(x) {
        return this.nombreRationnel.estEgal(x);
    }
}
class RationnelOperationParQuotient extends RationnelDeleguantAEtat {
    constructor(nombreRationnel) {
        super(nombreRationnel);
    }
    creerParFraction(f) {
        return new RationnelOperationParQuotient(fraction(f));
    }
    creerParNombre(q) {
        return new RationnelOperationParQuotient(nombre(q));
    }
    somme(x) {
        return this.creerParNombre(this.valeur() + x.valeur());
    }
    zero() {
        return this.creerParNombre(0);
    }
    produit(x) {
        return this.creerParNombre(this.valeur() * x.valeur());
    }
    un() {
        return this.creerParNombre(1);
    }
    oppose() {
        return this.creerParNombre(-this.valeur());
    }
    inverse() {
        return this.creerParNombre(1 / this.valeur());
    }
    estEgal(x) {
        return this.valeur() === x.valeur();
    }
    representation() {
        return String(this.valeur());
    }
}
class RationnelOperationParFraction extends RationnelDeleguantAEtat {
    constructor(nombreRationnel) {
        super(nombreRationnel);
    }
    creerParFraction(f) {
        return new RationnelOperationParFraction(fraction(f));
    }
    creerParNombre(q) {
        return new RationnelOperationParFraction(nombre(q));
    }
    somme(x) {
        return this.creerParFraction([
            this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
    }
    zero() {
        return this.creerParFraction([0, 1]);
    }
    produit(x) {
        return this.creerParFraction([
            this.fraction()[0] * x.fraction()[0],
            this.fraction()[1] * x.fraction()[1]
        ]);
    }
    un() {
        return this.creerParFraction([1, 1]);
    }
    oppose() {
        return this.creerParFraction([
            -this.fraction()[0],
            this.fraction()[1]
        ]);
    }
    inverse() {
        return this.creerParFraction([
            this.fraction()[1],
            this.fraction()[0]
        ]);
    }
    estEgal(x) {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation() {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}
/*
 * Implementation d'une fabrique de fractions avec une classe anonyme * par adaptation.
 */
function fractionDimorphe(frac) {
    return new class {
        estFraction() {
            return true;
        }
        estQuotient() {
            return false;
        }
        autreRepresentation() {
            return quotientDimorphe(frac[0] / frac[1]);
        }
        valeur() {
            return frac[0] / frac[1];
        }
        fraction() {
            return frac;
        }
        creerParFraction(f) {
            return fractionDimorphe(f);
        }
        creerParNombre(q) {
            return fractionDimorphe(approximation_1.approximation(q));
        }
        representation() {
            return String(this.valeur());
        }
        estEgal(x) {
            const f = frac;
            const g = x;
            return f[0] * g.fraction()[1] === f[1] * g.fraction()[0];
        }
    };
}
exports.fractionDimorphe = fractionDimorphe;
function quotientDimorphe(val) {
    return new class {
        estFraction() {
            return true;
        }
        estQuotient() {
            return false;
        }
        autreRepresentation() {
            return fractionDimorphe(approximation_1.approximation(val));
        }
        valeur() {
            return val;
        }
        fraction() {
            return approximation_1.approximation(val);
        }
        creerParFraction(f) {
            return quotientDimorphe(f[0] / f[1]);
        }
        creerParNombre(q) {
            return quotientDimorphe(q);
        }
        representation() {
            return String(this.valeur());
        }
        estEgal(x) {
            return val == x.valeur();
        }
    };
}
exports.quotientDimorphe = quotientDimorphe;
// Strategie de calcul par fractions
exports.calculsParFraction = new class {
    creerParNombre(q) {
        return fractionDimorphe(approximation_1.approximation(q));
    }
    creerParFraction(f) {
        return fractionDimorphe(f);
    }
    somme(x, y) {
        return this.creerParFraction([
            x.fraction()[0] * y.fraction()[1] + x.fraction()[1] * y.fraction()[0],
            x.fraction()[1] * y.fraction()[1]
        ]);
    }
    zero() {
        return this.creerParFraction([0, 1]);
    }
    produit(x, y) {
        return this.creerParFraction([
            x.fraction()[0] * y.fraction()[0],
            x.fraction()[1] * y.fraction()[1]
        ]);
    }
    un() {
        return this.creerParFraction([1, 1]);
    }
    oppose(x) {
        return this.creerParFraction([
            -x.fraction()[0],
            x.fraction()[1]
        ]);
    }
    inverse(x) {
        return this.creerParFraction([
            x.fraction()[1],
            x.fraction()[0]
        ]);
    }
};
function monRationnelPolymorphe(etat, strategie) {
    return new class {
        etat() {
            return etat;
        }
        estFraction() {
            return etat.estFraction();
        }
        estQuotient() {
            return etat.estQuotient();
        }
        modifierRepresentation() {
            etat.autreRepresentation();
        }
        modifierStrategie(s) {
            strategie = s;
        }
        fraction() {
            return etat.fraction();
        }
        valeur() {
            return etat.valeur();
        }
        creerParFraction(f) {
            return monRationnelPolymorphe(etat.creerParFraction(f), strategie);
        }
        creerParNombre(q) {
            return monRationnelPolymorphe(etat.creerParNombre(q), strategie);
        }
        somme(x) {
            return monRationnelPolymorphe(strategie.somme(x.etat(), etat), strategie);
        }
        zero() {
            return monRationnelPolymorphe(strategie.zero(), strategie);
        }
        produit(x) {
            return monRationnelPolymorphe(strategie.produit(x.etat(), etat), strategie);
        }
        un() {
            return monRationnelPolymorphe(strategie.un(), strategie);
        }
        oppose() {
            return monRationnelPolymorphe(strategie.oppose(etat), strategie);
        }
        inverse() {
            return monRationnelPolymorphe(strategie.inverse(etat), strategie);
        }
        representation() {
            return etat.representation();
        }
        estEgal(x) {
            return etat.estEgal(x);
        }
    };
}
exports.monRationnelPolymorphe = monRationnelPolymorphe;
//# sourceMappingURL=rationnelsEnCouches_eleves.js.map