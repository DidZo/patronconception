"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adaptorList = exports.CONS = exports.VIDE = exports.VisiteurPrimitifRecursif = exports.VisiteurRecursif = exports.VisiteurIteratif = void 0;
const listes_1 = require("../bibliotheque/listes");
const listes_2 = require("../bibliotheque/listes");
const listes_3 = require("../bibliotheque/listes");
const listes_4 = require("../bibliotheque/listes");
const listes_5 = require("../bibliotheque/listes");
const listes_6 = require("../bibliotheque/listes");
function VisiteurIteratif() {
    return new class {
        visiterVide() {
            return 0;
        }
        visiterCons(tete, resultatPrecedent) {
            return resultatPrecedent + 1;
        }
    };
}
exports.VisiteurIteratif = VisiteurIteratif;
function VisiteurRecursif() {
    return new class {
        visiterVide() {
            return 0;
        }
        visiterCons(tete, resultatPrecedent) {
            return resultatPrecedent + 1;
        }
    };
}
exports.VisiteurRecursif = VisiteurRecursif;
function VisiteurPrimitifRecursif() {
    return new class {
        visiterVide() {
            return 0;
        }
        visiterCons(tete, reste, resultatPrecedent) {
            return resultatPrecedent + 1;
        }
    };
}
exports.VisiteurPrimitifRecursif = VisiteurPrimitifRecursif;
class ListeIterable {
    accueilIteratif(v) {
        let res = v.visiterVide();
        for (let e of this) {
            res = v.visiterCons(e, res);
        }
        return res;
    }
    [Symbol.iterator]() {
        const original = this;
        return new class {
            constructor() {
                this.liste = original;
            }
            next() {
                if (this.liste.estVide()) {
                    return {
                        value: null,
                        done: true
                    };
                }
                const tete = this.liste.tete();
                this.liste = this.liste.reste();
                return {
                    value: tete,
                    done: false
                };
            }
        };
    }
}
function VIDE() {
    return new class extends ListeIterable {
        estVide() {
            return true;
        }
        tete() {
            throw new Error("Erreur : tete() sur liste vide !");
        }
        reste() {
            throw new Error("Erreur : reste() sur liste vide !");
        }
        vide() {
            return VIDE();
        }
        cons(t, r) {
            return CONS(t, r);
        }
        taille() {
            return 0;
        }
        accueilRecursif(v) {
            return v.visiterVide();
        }
        filtrage(casVide, casCons) {
            return casVide();
        }
        accueilPrimitifRecursif(casVide, casCons) {
            return casVide();
        }
    };
}
exports.VIDE = VIDE;
function CONS(t, r) {
    return new class extends ListeIterable {
        constructor() {
            super(...arguments);
            this._taille = 1 + r.taille();
        }
        estVide() {
            return false;
        }
        tete() {
            return t;
        }
        reste() {
            return r;
        }
        vide() {
            return VIDE();
        }
        cons(t, r) {
            return CONS(t, r);
        }
        taille() {
            return this._taille;
        }
        accueilRecursif(v) {
            return v.visiterCons(this.tete(), this.reste().accueilRecursif(v));
        }
        accueilPrimitifRecursif(casVide, casCons) {
            return casCons(this.tete(), this.reste());
        }
        filtrage(casVide, casCons) {
            return casCons(this.tete(), this.reste());
        }
    };
}
exports.CONS = CONS;
function adaptorList(adaptedListe) {
    return new class extends ListeIterable {
        estVide() {
            return listes_1.estVide(adaptedListe);
        }
        tete() {
            return listes_2.tete(adaptedListe);
        }
        reste() {
            return adaptorList(listes_3.reste(adaptedListe));
        }
        vide() {
            return adaptorList(listes_6.VIDE());
        }
        cons(t, r) {
            let tableau = new Array(t);
            let currentList = r;
            while (!currentList.estVide()) {
                tableau.push(currentList.tete());
            }
            return adaptorList(listes_5.tableauCommeListe(tableau));
        }
        taille() {
            return listes_4.listeCommeTableau(adaptedListe).length;
        }
        accueilRecursif(v) {
            if (this.estVide()) {
                return v.visiterVide();
            }
            else {
                return v.visiterCons(this.tete(), this.reste().accueilRecursif(v));
            }
        }
        filtrage(casVide, casCons) {
            if (this.estVide()) {
                return casVide();
            }
            else {
                return casCons(this.tete(), this.reste());
            }
        }
        accueilPrimitifRecursif(casVide, casCons) {
            if (this.estVide()) {
                return casVide();
            }
            else {
                return casCons(this.tete(), this.reste());
            }
        }
    };
}
exports.adaptorList = adaptorList;
let fonctionFiltree = (list, element) => {
    console.log(list.tete());
    return list.tete();
};
let list = VIDE();
list = CONS(42, list);
list = CONS(44, list);
list = CONS(57, list);
list = CONS(65, list);
let visiteurIteratif = VisiteurIteratif();
//console.log(list.accueilIteratif<number>(visiteurIteratif));
let visiteurRecursif = VisiteurRecursif();
//console.log(list.accueilRecursif<number>(visiteurRecursif));
let casVide = () => 0;
let casCons = (t, r) => {
    return 1 + r.accueilPrimitifRecursif(casVide, casCons);
};
let visiteurPrimitifRecursif = VisiteurPrimitifRecursif();
//console.log(list.accueilPrimitifRecursif<number>( casVide,casCons));
function taille(liste) {
    return liste.filtrage(// filtrage ou pattern matching
    casVide, casCons);
}
//console.log(f(list));
let adaptatorList = adaptorList(listes_5.tableauCommeListe(new Array(1, 2, 3, 4)));
console.log(adaptatorList.accueilIteratif(visiteurIteratif));
console.log(adaptatorList.accueilRecursif(visiteurRecursif));
console.log(taille(adaptatorList));
//# sourceMappingURL=listes.js.map