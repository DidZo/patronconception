// Fonction permettant d'hériter de tous les champs.
export function heriterInterfacesConcretes(descendant: any, ascendants: any[]) {
    ascendants.forEach(ascendant => {
        Object.getOwnPropertyNames(ascendant.prototype).forEach(nom => {
            descendant.prototype[nom] = ascendant.prototype[nom];
        });
    });
}


// Fonctions permettant l'héritage d'un champ qu'on doit nommer.
// - usage : this.f = heritage(f, Classe);
// - sans contrôle de typage (à la js)
export function heritage(nomChamp: string, classeParente: any) {
    return classeParente.prototype[nomChamp];
}
// - usage : this.f = heritageType<SousType, SurType>('f', SurType)
//   (SousType < SurType, 'f' champ de SurType)
// - avec contrôle de typage (à la ts)
export function heritageType<C extends P, P>(nomChamp: keyof P,
    classeParente: any)
    : any {
    return classeParente.prototype[nomChamp];
}