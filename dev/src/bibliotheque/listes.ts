import { Unite } from "./types";

export type Liste<T> = { vide: Unite } | readonly [T, Liste<T>];

export function estVide<T>(l: Liste<T>): l is { vide: Unite } {
    return ("vide" in l);
}

export function filtrage<T, R>(
    l: Liste<T>,
    casVide: () => R,
    casCons: (t: T, r: Liste<T>) => R): R {
    if (estVide(l)) {
        return casVide();
    } else {
        return casCons(l[0], l[1]);
    }
}

export function visiteFonctionIterative<T, R>(
    l: Liste<T>,
    casVide: () => R,
    casCons: (t: T, r: R) => R): R {
    if (estVide(l)) {
        return casVide();
    } else {
        return casCons(
            l[0], visiteFonctionIterative(l[1], casVide, casCons));
    }
}

export function visiteFonctionRecursivePrimitive<T, R>(
    l: Liste<T>,
    casVide: () => R,
    casCons: (t: T, reste : Liste<T>, r: R) => R): R {
    if (estVide(l)) {
        return casVide();
    } else {
        return casCons(
            l[0], l[1], visiteFonctionRecursivePrimitive(l[1], casVide, casCons));
    }
}


const VIDE_singleton: { vide: Unite } = { vide: "un" };

export function VIDE<E>(): Liste<E> {
    return VIDE_singleton;
}

export function tete<T>(l: Liste<T>): T {
    if (estVide(l))
        throw new Error('tête indéfinie : liste vide')
    return l[0]
}

export function reste<T>(l: Liste<T>): Liste<T> {
    if (estVide(l))
        throw new Error('tête indéfinie : liste vide')
    return l[1]
}

export function concatenation<T>(l: Liste<T>, k: Liste<T>): Liste<T> {
    let m: Liste<T> = VIDE()
    while (!estVide(l)) {
        m = [l[0], m]
        l = l[1]
    }
    while (!estVide(m)) {
        k = [m[0], k]
        m = m[1]
    }
    return k
}

export function tailleRecursive<T>(l: Liste<T>): number {
    return filtrage(l,
        () => 0,
        (t, fdfse) => 1 + tailleRecursive(l));
}

export function listeCommeTableau<T>(l: Liste<T>): Array<T> {
    let res = []
    while (!estVide(l)) {
        res.push(l[0])
        l = l[1]
    }
    return res;
}

export function tableauCommeListe<T>(tab: Array<T>): Liste<T> {
    let l: Liste<T> = VIDE()
    for (let i = tab.length - 1; i >= 0; i--) {
        l = [tab[i], l]
    }
    return l;
}

export function DEF<T>(...args : T[]) : Liste<T>{
    return tableauCommeListe(args);
}