export interface Representable {
    representation() : string;
}

export interface Identifiable<T> {
    estEgal(x: T): boolean;
}


