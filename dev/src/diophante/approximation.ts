/*
 * Comment approcher un réel par une fraction ?
 * On souhaite trouver une méthode 
 * - qui approche l'ensemble des flottants, avec une
 *  bonne précision relative, 
 * - qui associe à un réel rationnel p/q cette même fraction, ou 
 *   mieux sa forme réductible.
 * La solution naïve de multiplier par une puissance de 10 et de 
 * prendre la partie entière puis de diviser par cette même 
 * puissance ne fonctionne pas. Elle ne couvre pas l'ensemble des 
 * flottants (par exemple ceux inférieurs à l'inverse du facteur
 *  multiplicatif) et ne permet pas de retrouver une fraction donnée.
 * 
 * L'approximation diophantienne fournit la méthode recherchée.  
 * r : réel, E(r) : partie entière de r, D(r) : partie décimale
 * r = r(0) = E(r) + D(r) 
 *        = E(r) si D(r) = 0 (terminé)
 *        = E(r) + 1/(1 / D(r)) si D(r) != 0
 * 1 / D(r) = r(1) = E(r(1)) + D(r(1)) etc.
 * -> Développement en fraction continue
 * -> Approximation au rang n en prenant D(r(n)) = 0 
 *    (convergente vers r(0))
 * -> Approximation par une fraction h(n) / k(n)
 * -> Relation de récurrence entre les couples h(n) / k(n)
 * - voir https://fr.wikipedia.org/wiki/Fraction_continue
 * - h(-2) / k(-2) = 0 / 1 (notation symbolique pour un couple)
 * - h(-1) / k(-1) = 1 / 0 
 * - h(n)  / k(n)  = 
 *     E(r(n)) * h(n-1) + h(n-2) / E(r(n)) * k(n-1) + k(n-2)  
 * Calcul par itérations successives
 * - arrêt lorsque l'approximation relative est suffisante ou lorsque la 
 *   partie décimale est trop petite. 
 */
type Etat = {
    r: number,
    h2: number,
    h1: number,
    k2: number,
    k1: number
};

const MODE_VERBEUX = true;
const PRECISION = 1e-15; // Précision relative
const INVERSE_MAX_INT = 1.1 / Number.MAX_SAFE_INTEGER;
/*
 * Précondition : Abs(e.r - e.p) > INVERSE_MAX_INT
 */
function suivant(e: Etat): Etat {
    let p = Math.floor(e.r);
    return {
        r: 1.0 / (e.r - p), // inverse de la partie décimale
        // équations de récurrence
        h2: e.h2 * p + e.h1,
        h1: e.h2,
        k2: e.k2 * p + e.k1,
        k1: e.k2
    };
}

function numerateur(e: Etat): number {
    return suivant(e).h2;
}
function denominateur(e: Etat): number {
    return suivant(e).k2;
}

export function approximation(r: number): [number, number] {
    const abs = Math.abs(r);
    const signe = (r >= 0);
    let courant = {
        r: abs,
        h2: 1,
        h1: 0,
        k2: 0,
        k1: 1
    };
    let n = 0;
    while (estRelativementTropLoin(abs,
        numerateur(courant), denominateur(courant))
        && partieDecimaleAssezGrande(courant.r)) {
        courant = suivant(courant);
        n++;
    }
    if (MODE_VERBEUX)
        console.log("- Profondeur : " + n);
    let h = numerateur(courant);
    let k = denominateur(courant);

    h = signe ? h : -h;
    return [h, k];
}

function partieDecimaleAssezGrande(r: number): boolean {
    let p = Math.floor(r);
    let condition = (r - p) > INVERSE_MAX_INT;
    if (MODE_VERBEUX && !condition) {
        console.log("- Dépassement : " + (r - p));
    }
    return condition;
}

function estRelativementTropLoin(r: number, h: number, k: number) {
    let condition = Math.abs(r - h / k) > r * PRECISION;
    if (MODE_VERBEUX && !condition) {
        console.log("- Bonne approximation : "
            + Math.abs(r - h / k) + " / " + r);
    }
    return condition;
}


