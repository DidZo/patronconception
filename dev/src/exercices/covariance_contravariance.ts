// Variance ?
interface Liste1<T> {
    tete(): T;
    reste(): Liste1<T>;
}
// Variance ?
interface Liste2<T> {
    tete(): T;
    reste(): Liste4<T>;
    cons(t: T): Liste4<T>; // fabrique
}
// Variance ?
interface Liste3<T> {
    tete(): T;
    reste(): Liste4<T>;
    cons : (t: T) => Liste4<T>; // fabrique
}

// Variance ?
interface Liste4<T> {
    tete(): T;
    reste(): Liste4<T>;
    cons : <E>(t: E, r : Liste4<E>) => Liste4<E>; // fabrique
}
// Variance ?
interface Liste5<T> {
    setTete(t: T): void
    reste(): Liste5<T>;
}
// Variance ?
interface Liste6<T> {
    setTete: (t: T) => void
    reste(): Liste5<T>;
}

// Variance ?
interface Liste7<T> {
    tete(): T;
    setTete(t: T): void
    reste(): Liste7<T>;
    cons(t: T): Liste7<T>;
}




////////////////////////////////


class A { }
class B extends A {
    f(): void { }
}

function covariance1(l: Liste1<B>): Liste1<A> {
    return l; // Oui car B extends A, donc B inclue A
}
function contravariance1(l: Liste1<A>): Liste1<B> {
        return l; // Non car A n'inclue pas B
}

function covariance2(l: Liste2<B>): Liste2<A> {
    return l;   // Oui car B extends A, donc B inclue A
}
function contravariance2(l: Liste2<A>): Liste2<B> {
    return l; // Non car A n'inclue pas B
}

function covariance3(l: Liste3<B>): Liste3<A> {
    return l;  // Non car il y a contravariance pour l'argument de =>
}
function contravariance3(l: Liste3<A>): Liste3<B> {
    return l;  // Non car il y a covariance pour le résultat de =>
}

function covariance4(l: Liste4<B>): Liste4<A> {
    return l; //Oui car B extends A, donc B inclue A
}
function contravariance4(l: Liste4<A>): Liste4<B> {
    return l; // Non car A n'inclue pas B
}

function covariance5(l: Liste5<B>): Liste5<A> {
    return l;
}
function contravariance5(l: Liste5<A>): Liste5<B> {
    return l;
}
// Explication : TODO - Correct ? oui/non
function covariance6(l: Liste6<B>): Liste6<A> {
    return l;
}
function contravariance6(l: Liste6<A>): Liste6<B> {
    return l;
}
// Explication : TODO - Correct ? oui/non
function covariance7(l: Liste7<B>): Liste7<A> {
    return l;
}
function contravariance7(l: Liste7<A>): Liste7<B> {
    return l;
}