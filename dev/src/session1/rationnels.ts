import { Identifiable, Representable } from "../bibliotheque/proprietes";
import { approximation } from "../diophante/approximation";
import { Corps } from "../structuresAlgebriques/hierarchie";

export interface Fraction {
    fraction(): [number, number];
}

export interface Nombre {
    valeur(): number;
}
export interface FabriqueRationnel<T> {
    creerParFraction(f: [number, number]): T;
    creerParNombre(q: number): T;
}
// Remarque. On pourrait remplacer Identifiable<Rationnel> par 
// Identifiable<Fraction & Nombre> : le remplacement par un sur-type 
// permettrait de tester l'égalité avec plus d'objets, tous ayant les 
// accesseurs d'un rationnel. Bien noter l'usage de &, pour 
// l'intersection.
export interface Rationnel
    extends Fraction, Nombre, FabriqueRationnel<Rationnel>, Corps<Rationnel>, Representable, Identifiable<Rationnel> {
}

class RationnelParFraction implements Rationnel {
    constructor(private numerateur_denominateur: [number, number]) { }

    fraction(): [number, number] {
        return this.numerateur_denominateur;
    }
    valeur(): number {
        return this.numerateur_denominateur[0] / this.numerateur_denominateur[1];
    }
    creerParFraction(f: [number, number]): Rationnel {
        return new RationnelParFraction(f);
    }
    creerParNombre(q: number): Rationnel {
        return new RationnelParFraction(approximation(q));
    }
    somme(x: Rationnel): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);
    }
    zero(): Rationnel {
        return this.creerParFraction([0, 1]);
    }
    produit(x: Rationnel): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[0] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);

    }
    un(): Rationnel {
        return this.creerParFraction([1, 1]);
    }
    oppose(): Rationnel {
        return this.creerParFraction(
            [
                -this.fraction()[0]
                ,
                this.fraction()[1]
            ]);
    }
    inverse(): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[1]
                ,
                this.fraction()[0]
            ]);
    }
    estEgal(x: Rationnel): boolean {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation(): string {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}

class RationnelParQuotient implements Rationnel {
    constructor(private quotient: number) { }

    fraction(): [number, number] {
        return approximation(this.quotient);
    }
    valeur(): number {
        return this.quotient;
    }
    creerParFraction(f: [number, number]): Rationnel {
        return new RationnelParQuotient(f[0] / f[1]);
    }
    creerParNombre(q: number): Rationnel {
        return new RationnelParQuotient(q);
    }
    somme(x: Rationnel): Rationnel {
        return this.creerParNombre(this.valeur() + x.valeur());
    }
    zero(): Rationnel {
        return this.creerParNombre(0);
    }
    produit(x: Rationnel): Rationnel {
        return this.creerParNombre(this.valeur() * x.valeur());
    }
    un(): Rationnel {
        return this.creerParNombre(1);
    }
    oppose(): Rationnel {
        return this.creerParNombre(-this.valeur());
    }
    inverse(): Rationnel {
        return this.creerParNombre(1 / this.valeur());
    }
    estEgal(x: Rationnel): boolean {
        return this.valeur() === x.valeur();
    }
    representation(): string {
        return String(this.valeur());
    }
}

class RationnelParQuotientEfficace extends RationnelParQuotient implements Rationnel {  
    private _fraction : [number, number];
    constructor(quotient: number) { 
        super(quotient);
        this._fraction = [0, 0];
    }

    fraction(): [number, number] {
        if(this._fraction[1] == 0) 
            this._fraction = super.fraction(); 
        return this._fraction;
    }
}



export const FABRIQUE_RationnelParFraction: FabriqueRationnel<Rationnel> = new RationnelParFraction([0, 1]);

export const FABRIQUE_RationnelParQuotient: FabriqueRationnel<Rationnel> = new RationnelParQuotient(0);

let x : RationnelParQuotientEfficace 
    = new RationnelParQuotientEfficace(5 / 3);
console.log(x.fraction());
console.log(x.fraction());
let y : Rationnel = x; 
console.log(y.fraction());
