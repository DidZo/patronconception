import { Identifiable, Representable } from "../bibliotheque/proprietes";
import { approximation } from "../diophante/approximation";
import { Corps } from "../structuresAlgebriques/hierarchie";

import {
    Fraction, Nombre, FabriqueRationnel
} from "./rationnels"

export interface RationnelMutable
    extends 
    Fraction, Nombre, FabriqueRationnel<RationnelMutable>,  Corps<RationnelMutable>, Representable, Identifiable<RationnelMutable> 
    {
    changerFraction(f: [number, number]): void;
    changerValeur(q: number): void;
}

class RationnelParFraction implements RationnelMutable {
    constructor(private numerateur_denominateur: [number, number]) { }
    changerFraction(f: [number, number]): void {
        this.numerateur_denominateur = f;
    }
    changerValeur(q: number): void {
        this.changerFraction(approximation(q));
    }
    fraction(): [number, number] {
        return this.numerateur_denominateur;
    }
    valeur(): number {
        return this.numerateur_denominateur[0] / this.numerateur_denominateur[1];
    }
    creerParFraction(f: [number, number]): RationnelMutable {
        return new RationnelParFraction(f);
    }
    creerParNombre(q: number): RationnelMutable {
        return new RationnelParFraction(approximation(q));
    }
    somme(x: RationnelMutable): RationnelMutable {
        this.changerFraction(
            [
                this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);
        return this;
    }
    zero(): RationnelMutable {
        this.changerFraction([0, 1]);
        return this;
    }
    produit(x: RationnelMutable): RationnelMutable {
        this.changerFraction(
            [
                this.fraction()[0] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);
        return this;
    }
    un(): RationnelMutable {
        this.changerFraction([1, 1]);
        return this;
    }
    oppose(): RationnelMutable {
        this.changerFraction(
            [
                -this.fraction()[0]
                ,
                this.fraction()[1]
            ]);
        return this;
    }
    inverse(): RationnelMutable {
        this.changerFraction(
            [
                this.fraction()[1]
                ,
                this.fraction()[0]
            ]);
        return this;
    }
    estEgal(x: RationnelMutable): boolean {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation(): string {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}

class RationnelParQuotient implements RationnelMutable {
    constructor(private quotient: number) { }
    changerFraction(f: [number, number]): void {
        this.changerValeur(f[0] / f[1]);
    }
    changerValeur(q: number): void {
        this.quotient = q;
    }
    fraction(): [number, number] {
        return approximation(this.quotient);
    }
    valeur(): number {
        return this.quotient;
    }
    creerParFraction(f: [number, number]): RationnelMutable {
        return new RationnelParQuotient(f[0] / f[1]);
    }
    creerParNombre(q: number): RationnelMutable {
        return new RationnelParQuotient(q);
    }
    somme(x: RationnelMutable): RationnelMutable {
        this.changerValeur(this.valeur() + x.valeur());
        return this
    }
    zero(): RationnelMutable {
        this.changerValeur(0);
        return this
    }
    produit(x: RationnelMutable): RationnelMutable {
        this.changerValeur(this.valeur() * x.valeur());
        return this
    }
    un(): RationnelMutable {
        this.changerValeur(1);
        return this
    }
    oppose(): RationnelMutable {
        this.changerValeur(-this.valeur());
        return this
    }
    inverse(): RationnelMutable {
        this.changerValeur(1 / this.valeur());
        return this
    }
    estEgal(x: RationnelMutable): boolean {
        return this.valeur() === x.valeur();
    }
    representation(): string {
        return String(this.valeur());
    }
}

export const FABRIQUE_RationnelParFraction: FabriqueRationnel<RationnelMutable> = new RationnelParFraction([0, 1]);

export const FABRIQUE_RationnelParQuotient: FabriqueRationnel<RationnelMutable> = new RationnelParQuotient(0);