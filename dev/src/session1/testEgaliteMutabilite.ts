import { 
    FABRIQUE_RationnelParFraction as fabF
} from "./rationnels";

import { 
    FABRIQUE_RationnelParFraction as fabFM
} from "./rationnelsMutables";

const r1 = fabF.creerParFraction([1, 3])
const r2 = fabF.creerParFraction([2, 3])
console.log("faux ? " + r1.estEgal(r1.somme(r2))) 
    //  Mathématiquement  : 1/3 = 1/3 + 2/3

const rm1 = fabFM.creerParFraction([1, 3])
const rm2 = fabFM.creerParFraction([2, 3])
console.log("faux ? " + rm1.estEgal(rm1.somme(rm2))) 
    //  Mathématiquement : 1/3 = 1/3 + 2/3
    //  Informatiquement : rationnel pointé par rm1 
    //              égal à rationnel pointé par rm1