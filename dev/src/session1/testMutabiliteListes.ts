// dev> ts-node -P src/tsconfig.json src/session1/X.ts 
// après installation de perf_hooks (indication donnée)

/*
 * Comparaison entre un tableua et une liste chaînée
 * 
 */
import modulePerf = require('perf_hooks');
import { concatenation, Liste, listeCommeTableau, tableauCommeListe, tete, VIDE } from '../bibliotheque/listes';

const max = 20_000

let t1 = modulePerf.performance.now();

let sm = [];

for (let i = 0; i < max; i++) {
    sm.push('a')
}
console.log(sm[max - 1]);

let t2 = modulePerf.performance.now();
console.log("Test tableaux mutables (temps en ms) : " + (t2 - t1));

t1 = modulePerf.performance.now();

let si: Liste<string> = VIDE()

for (let i = 0; i < max; i++) {
    si = concatenation(si, ['a', VIDE()])
}
console.log(tete(si));

t2 = modulePerf.performance.now();
console.log("Test listes immutables (temps en ms) : " + (t2 - t1));

function fonctionPenalisante(l: Liste<string>): Liste<string> {
    for (let i = 0; i < max; i++) {
        l = concatenation(l, ['a', VIDE()])
    }
    return l
}

function fonctionPlusEfficace(l: Liste<string>): Liste<string> {
    let tab = listeCommeTableau(l)
    for (let i = 0; i < max; i++) {
        tab.push('a')
    }
    return tableauCommeListe(tab)
}

t1 = modulePerf.performance.now();
fonctionPenalisante(VIDE())
t2 = modulePerf.performance.now();
console.log("Test fonction pénalisante (temps en ms) : " + (t2 - t1));

t1 = modulePerf.performance.now();
fonctionPlusEfficace(VIDE())
t2 = modulePerf.performance.now();
console.log("Test fonction plus efficace (temps en ms) : " + (t2 - t1));
