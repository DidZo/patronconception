import { 
    FABRIQUE_RationnelParFraction as fabF, FABRIQUE_RationnelParQuotient as fabQ 
} from "./rationnels"
let n = fabQ.creerParNombre(5 / 3)
console.log(n.representation())
n = fabQ.creerParFraction([5, 3])
console.log(n.representation())
let m = fabF.creerParFraction([3, 2])
console.log(m.representation())
m = fabF.creerParNombre(3 / 2)
console.log(m.representation())
console.log(m.somme(n).representation())
console.log(n.somme(m).representation())
console.log(m.produit(n).representation())
console.log(n.produit(m).representation())

m = fabF.creerParNombre(Math.PI)
console.log(m.representation())
