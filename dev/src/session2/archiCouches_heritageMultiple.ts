import { heritageType } from "../bibliotheque/heritage";

interface Abstrait {
    f(): void;
}

// Classe donnant une implémentation par défaut
abstract class Abstrait_defaut implements Abstrait {
    f(): void {
        console.log("Abstrait.f")
    }
}

interface Concret {
    g(): void;
}

class ImplemConcret implements Concret {
    g() {
        console.log("Concret.g")
    }
}

class Implem extends ImplemConcret implements Concret, Abstrait {
    // Déclaration nécessaire sous la forme d'attributs fonctionnels
    // -> Répétition des déclarations d'Abstrait
    public readonly f: () => void;
    constructor() {
        super();
        // Agrégation du code
        this.f = heritageType<Implem, Abstrait_defaut>(
            'f', Abstrait_defaut);
    }
}

let c = new Implem();
c.f();
c.g();
let c1 = new Implem();
console.log("identité des méthodes ? " + (c.f === c1.f))
// Conclusion : partage du code !

