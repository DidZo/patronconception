import {
    Canal, AgentCommuniquant
} from "./architecture";

import {
    emettre as EM,
    envoyerApresEncapsulation as ENCAP,
    envoyerApresDecoupage as DEC
} from "./communication"

class CanalOutProtocole1 implements Canal {
    emettre(msg: string): void {
        EM(1, msg);
    }

}

class CanalOutProtocole2 implements Canal {
    emettre(msg: string): void {
        EM(2, msg);
    }

}

abstract class AgentDeleguantACanal implements AgentCommuniquant {
    // Attribut de type Canal et constructeur associé
    constructor(private canal: Canal) { }
    abstract envoyer(msg: string): void;
    emettre(msg: string): void {
        this.canal.emettre(msg);
    }

}

class AgentDecoupantMessages extends AgentDeleguantACanal implements AgentCommuniquant {
    constructor(canal: Canal) {
        super(canal);
    }

    envoyer(msg: string) {
        DEC(this, msg);
    }

}

class AgentEncapsulantMessages extends AgentDeleguantACanal implements AgentCommuniquant {
    constructor(canal: Canal) {
        super(canal);
    }

    envoyer(msg: string) {
        ENCAP(this, msg);
    }
}

let a = new AgentDecoupantMessages(new CanalOutProtocole1());
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentDecoupantMessages(new CanalOutProtocole2());
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessages(new CanalOutProtocole1());
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessages(new CanalOutProtocole2());
a.envoyer("nul n'est censé ignorer les principes de modularité.");
