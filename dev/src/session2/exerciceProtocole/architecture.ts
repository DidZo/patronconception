export interface Agent {
    envoyer(msg : string) : void;
}

export interface Canal {
    emettre(msg : string) : void;
}

export interface AgentCommuniquant extends Agent, Canal {}