import {
    Canal, AgentCommuniquant
} from "./architecture";

import {
    emettre as EM,
    envoyerApresEncapsulation as ENCAP,
    envoyerApresDecoupage as DEC
} from "./communication"

class CanalOutProtocole1 implements Canal {
    emettre(msg: string): void {
        EM(1, msg);
    }
    
}

class CanalOutProtocole2 implements Canal {
    emettre(msg: string): void {
        EM(2, msg);
    }
    
}

class AgentEncapsulantMessagesPourProtocole1 extends CanalOutProtocole1 implements AgentCommuniquant {
    envoyer(msg: string): void {
        ENCAP(this, msg);
    }
}

class AgentEncapsulantMessagesPourProtocole2 extends CanalOutProtocole2 implements AgentCommuniquant {
    envoyer(msg: string): void {
        ENCAP(this, msg);
    }
}

class AgentDecoupantMessagesPourProtocole1 extends CanalOutProtocole1 implements AgentCommuniquant {
    envoyer(msg: string): void {
        DEC(this, msg);
    }
}

class AgentDecoupantMessagesPourProtocole2 extends CanalOutProtocole2 implements AgentCommuniquant {
    envoyer(msg: string): void {
        DEC(this, msg);
    }
}

let a = new AgentDecoupantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentDecoupantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");