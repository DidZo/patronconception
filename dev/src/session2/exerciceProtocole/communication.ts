export function emettre(numero : number, msg : string) : void {
    console.log("protocole " + numero + " : " + msg);
}

// Encapsulation
export function envoyerApresEncapsulation(canal : any, msg : string) {
    canal.emettre(msg);
}
// Découpage
export function envoyerApresDecoupage(canal : any, msg : string) {
    const TAILLE = 5;
    const q = msg.length / TAILLE;
    const r = msg.length % TAILLE;
    for(let j = 0; j < q; j++){
            canal.emettre(msg.substring(j * TAILLE, (j+1) * TAILLE));
    }
    canal.emettre(msg.substring(q * TAILLE, q * TAILLE + r));
}
