import {
    Canal, AgentCommuniquant
} from "./architecture";

import {
    emettre as EM,
    envoyerApresEncapsulation as ENCAP,
    envoyerApresDecoupage as DEC
} from "./communication"

abstract class AgentDecoupantMessages implements AgentCommuniquant {
    abstract emettre(msg: string): void;

    envoyer(msg: string) {
        DEC(this, msg);
    }

}

abstract class AgentEncapsulantMessages implements AgentCommuniquant {
    abstract emettre(msg: string): void;

    envoyer(msg: string) {
        ENCAP(this, msg);
    }

}

class AgentDecoupantMessagesPourProtocole1 extends AgentDecoupantMessages {
    emettre(msg: string): void {
        EM(1, msg);
    }
}
class AgentDecoupantMessagesPourProtocole2 extends AgentDecoupantMessages {
    emettre(msg: string): void {
        EM(2, msg);
    }
}
class AgentEncapsulantMessagesPourProtocole1 extends AgentEncapsulantMessages {
    emettre(msg: string): void {
        EM(1, msg);
    }
}

class AgentEncapsulantMessagesPourProtocole2 extends AgentEncapsulantMessages {
    emettre(msg: string): void {
        EM(2, msg);
    }
}

let a = new AgentDecoupantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentDecoupantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");