import { heritageType } from "../../bibliotheque/heritage";
import {
    Canal, AgentCommuniquant
} from "./architecture";

import {
    emettre as EM,
    envoyerApresEncapsulation as ENCAP,
    envoyerApresDecoupage as DEC
} from "./communication"

abstract class AgentDecoupantMessages implements AgentCommuniquant {
    abstract emettre(msg: string): void;

    envoyer(msg: string) {
        DEC(this, msg);
    }

}

abstract class AgentEncapsulantMessages implements AgentCommuniquant {
    abstract emettre(msg: string): void;

    envoyer(msg: string) {
        ENCAP(this, msg);
    }

}

class CanalOutProtocole1 implements Canal {
    emettre(msg: string): void {
        EM(1, msg);
    }

}

class CanalOutProtocole2 implements Canal {
    emettre(msg: string): void {
        EM(2, msg);
    }

}

class AgentDecoupantMessagesPourProtocole1 extends CanalOutProtocole1 implements AgentCommuniquant {
    envoyer: (msg: string) => void;
    constructor() {
        super();
        this.envoyer = AgentDecoupantMessages.prototype['envoyer'];//heritageType<AgentDecoupantMessagesPourProtocole1, AgentDecoupantMessages>('envoyer', AgentDecoupantMessages);
    }
}
class AgentDecoupantMessagesPourProtocole2 extends CanalOutProtocole2 implements AgentCommuniquant {
    envoyer: (msg: string) => void;
    constructor() {
        super();
        this.envoyer = AgentDecoupantMessages.prototype['envoyer'];
        //ou : heritageType<AgentDecoupantMessagesPourProtocole2,  
        //                  AgentDecoupantMessages>(
        //        'envoyer', AgentDecoupantMessages);
    } 
} 

class AgentEncapsulantMessagesPourProtocole1 extends CanalOutProtocole1 implements AgentCommuniquant {
    envoyer: (msg: string) => void;
    constructor() {
        super();
        this.envoyer = heritageType<AgentEncapsulantMessagesPourProtocole1, AgentEncapsulantMessages>('envoyer', AgentEncapsulantMessages);
    }
}

class AgentEncapsulantMessagesPourProtocole2 extends CanalOutProtocole2 implements AgentCommuniquant {
    envoyer: (msg: string) => void;
    constructor() {
        super();
        this.envoyer = heritageType<AgentEncapsulantMessagesPourProtocole1, AgentEncapsulantMessages>('envoyer', AgentEncapsulantMessages);
    }
}

let a = new AgentDecoupantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentDecoupantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessagesPourProtocole1();
a.envoyer("nul n'est censé ignorer les principes de modularité.");

a = new AgentEncapsulantMessagesPourProtocole2();
a.envoyer("nul n'est censé ignorer les principes de modularité.");