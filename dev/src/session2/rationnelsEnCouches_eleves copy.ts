import {Identifiable, Representable} from "../bibliotheque/proprietes";
import {approximation} from "../diophante/approximation";
import {FabriqueRationnel, Fraction, Nombre, Rationnel} from "../session1/rationnels";
import {AgentCommuniquant} from "./exerciceProtocole/architecture";
import {RationnelMutable} from "../session1/rationnelsMutables";

/* 
* 1. Couche basse : les nombres rationnels sans la structure  algébrique
* 
*/

// Remarque. Il est judicieux ici de remplacer 
// Identifiable<NombreRationnel> par 
// Identifiable<Fraction & Nombre> : le remplacement par un sur-type 
// permet de tester l'égalité avec plus d'objets, tous ayant les 
// accesseurs d'un nombre rationnel. Bien noter l'usage de &, pour 
// l'intersection. 
export interface NombreRationnel
    extends Nombre, Fraction, FabriqueRationnel<NombreRationnel>, Representable, Identifiable<Nombre & Fraction> {
}


/*
 * Implémentation d'une fabrique avec une classe anonyme.
 */

export function fraction(frac: [number, number]): NombreRationnel {
    return new class implements NombreRationnel {

        valeur(): number {
            return frac[0] / frac[1]
        }

        creerParFraction(f: [number, number]): NombreRationnel {
            return fraction(f);
        }

        creerParNombre(q: number): NombreRationnel {
            return fraction(approximation(q))
        }

        fraction(): [number, number] {
            return frac;
        }

        representation(): string {
            return this.fraction()[0]
                + "/" + this.fraction()[1];
        }

        estEgal(x: NombreRationnel): boolean {
            const f = this.fraction();
            const g = x.fraction();
            return f[0] * g[1] === f[1] * g[0];
        }
    };
};

/*
 * Implémentation d'une fabrique avec une classe anonyme.
 */
export function nombre(val: number): NombreRationnel {
    return new class implements NombreRationnel {

        fraction(): [number, number] {
            return approximation(val)
        }

        creerParFraction(f: [number, number]): NombreRationnel {
            return nombre(f[0] / f[1])
        }

        creerParNombre(q: number): NombreRationnel {
            return nombre(q)
        }

        valeur(): number {
            return val;
        }

        representation(): string {
            return String(this.valeur());
        }

        estEgal(x: NombreRationnel): boolean {
            return this.valeur() === x.valeur();
        }
    };
};

/*
* 2. A FAIRE - La couche haute : agrégation avec délégation
*/

abstract class RationnelDeleguantAEtat implements Rationnel {

    constructor(private nombreRationnel: NombreRationnel) {

    }

    fraction(): [number, number] {
        return this.nombreRationnel.fraction();
    }


    valeur(): number {
        return this.nombreRationnel.valeur();
    }

    representation(): string {
        return this.nombreRationnel.representation();
    }

    estEgal(x: RationnelMutable): boolean {
        return this.nombreRationnel.estEgal(x);
    }


    abstract creerParFraction(f: [number, number]): Rationnel;

    abstract creerParNombre(q: number): Rationnel;

    abstract inverse(): Rationnel;

    abstract oppose(): Rationnel;

    abstract produit(x: RationnelMutable): Rationnel;

    abstract somme(x: RationnelMutable): Rationnel;

    abstract un(): Rationnel;

    abstract zero(): Rationnel;

}

interface RationnelOperation {
    somme(x: Rationnel): Rationnel;
    zero(): Rationnel ;
    produit(x: Rationnel): Rationnel;
    un(): Rationnel;
    oppose(): Rationnel;
    inverse(): Rationnel ;
    estEgal(x:Rationnel): Boolean;
    representation(): string ;
}

class RationnelOperationParQuotient extends RationnelDeleguantAEtat implements RationnelOperation,EtatRationnel {

    constructor(nombreRationnel: NombreRationnel) {
        super(nombreRationnel);
    }

    creerParFraction(f: [number, number]): Rationnel {
        return new RationnelOperationParQuotient(fraction(f));
    }
    creerParNombre(q: number): Rationnel {
        return new RationnelOperationParQuotient(nombre(q));
    }
    somme(x: Rationnel): Rationnel {
        return this.creerParNombre(this.valeur() + x.valeur());
    }
    zero(): Rationnel {
        return this.creerParNombre(0);
    }
    produit(x: Rationnel): Rationnel {
        return this.creerParNombre(this.valeur() * x.valeur());
    }
    un(): Rationnel {
        return this.creerParNombre(1);
    }
    oppose(): Rationnel {
        return this.creerParNombre(-this.valeur());
    }
    inverse(): Rationnel {
        return this.creerParNombre(1 / this.valeur());
    }
    estEgal(x: Rationnel): boolean {
        return this.valeur() === x.valeur();
    }
    representation(): string {
        return String(this.valeur());
    }
}

class RationnelOperationParFraction extends RationnelDeleguantAEtat implements RationnelOperation,EtatRationnel {

    constructor(nombreRationnel: NombreRationnel) {
        super(nombreRationnel);
    }

    creerParFraction(f: [number, number]): Rationnel {
        return new RationnelOperationParFraction(fraction(f));
    }
    creerParNombre(q: number): Rationnel {
        return new RationnelOperationParFraction(nombre(q));
    }
    somme(x: Rationnel): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);
    }
   
    zero(): Rationnel {
        return this.creerParFraction([0, 1]);
    }
    produit(x: Rationnel): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[0] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);

    }
    un(): Rationnel {
        return this.creerParFraction([1, 1]);
    }
    oppose(): Rationnel {
        return this.creerParFraction(
            [
                -this.fraction()[0]
                ,
                this.fraction()[1]
            ]);
    }
    inverse(): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[1]
                ,
                this.fraction()[0]
            ]);
    }
    estEgal(x: Rationnel): boolean {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation(): string {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}
    
    
    interface EtatRationnel extends Rationnel {
        //méthode d'administration pour le changement d'état
    }

    class EtatRationnelConcret {
        ;
        
        constructor(private  etatRationnel:EtatRationnel) {
        }

        public setState( etatRationnelP:EtatRationnel):void {
            this.etatRationnel = etatRationnelP;
        }
        creerParFraction(f: [number, number]): Rationnel {
            return new RationnelOperationParFraction(fraction(f));
        }
        creerParNombre(q: number): Rationnel {
            return new RationnelOperationParFraction(nombre(q));
        }
        somme(x: Rationnel): Rationnel {
            return this.etatRationnel.somme(x);
        }
       
        zero(): Rationnel {
            return this.etatRationnel.zero();
        }
        produit(x: Rationnel): Rationnel {
            return this.etatRationnel.produit(x);
        }

        un(): Rationnel {
            return this.etatRationnel.un();
        }
        oppose(): Rationnel {
            return this.etatRationnel.oppose();

        }
        inverse(): Rationnel {
            return this.etatRationnel.inverse();

        }
        estEgal(x: Rationnel): boolean {
            return this.etatRationnel.estEgal(x);

        }
        representation(): string {
            return this.etatRationnel.representation();

        }
        
    }


    interface StrategyEtatRationnel extends NombreRationnel{}
    interface StrategyOperationRationnel extends RationnelOperation {}
    // TODO: une strategy n'est pas un rationnel.  A ne pas utiliser avec Etat
    class RationnelStrategy implements StrategyEtatRationnel,StrategyOperationRationnel{
        
        constructor(private  strategyEtatRationnel:StrategyEtatRationnel,private  strategyOperationRationnel:StrategyOperationRationnel) {
        }

        public setEtatStrategy(strategyEtatRationnelP:StrategyEtatRationnel):void{
            this.strategyEtatRationnel = strategyEtatRationnelP;
        }
        public setOperationStrategy(strategyOperationRationnelP:StrategyOperationRationnel):void{
            this.strategyOperationRationnel = strategyOperationRationnelP;
        }

        somme(x: Rationnel): Rationnel {
            return  this.strategyOperationRationnel.somme(x); // .somme(this.x)
        }
        zero(): Rationnel {
            return this.strategyOperationRationnel.zero();
        }
        produit(x: Rationnel): Rationnel {
            return this.strategyOperationRationnel.produit(x);
        }
        un(): Rationnel {
            return this.strategyOperationRationnel.un();
        }
        oppose(): Rationnel {
            return this.strategyOperationRationnel.oppose();
        }
        inverse(): Rationnel {
            return this.strategyOperationRationnel.inverse();
        }
        valeur(): number {
            return this.strategyEtatRationnel.valeur();
        }
        fraction(): [number, number] {
            return this.strategyEtatRationnel.fraction();
        }
        creerParFraction(f: [number, number]): NombreRationnel {
            return this.strategyEtatRationnel.creerParFraction(f);
        }
        creerParNombre(q: number): NombreRationnel {
            return this.strategyEtatRationnel.creerParNombre(q);
        }
        representation(): string {
            return this.strategyEtatRationnel.representation();
        }
        estEgal(x: Nombre & Fraction): boolean {
            return this.strategyEtatRationnel.estEgal(x);
        }
        
    }