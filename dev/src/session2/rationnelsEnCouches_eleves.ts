import {Identifiable, Representable} from "../bibliotheque/proprietes";
import {approximation} from "../diophante/approximation";
import {FabriqueRationnel, Fraction, Nombre, Rationnel} from "../session1/rationnels";
import {AgentCommuniquant} from "./exerciceProtocole/architecture";
import {RationnelMutable} from "../session1/rationnelsMutables";
import { Corps } from "../structuresAlgebriques/hierarchie";
import { ModuleCorps } from "../structuresAlgebriques/hierarchieModulaire";

/* 
* 1. Couche basse : les nombres rationnels sans la structure  algebrique
* 
*/
// Remarque. Il est judicieux ici de remplacer 
// Identifiable<NombreRationnel> par 
// Identifiable<Fraction & Nombre> : le remplacement par un sur-type 
// permet de tester l'egalite avec plus d'objets, tous ayant les 
// accesseurs d'un nombre rationnel. Bien noter l'usage de &, pour 
// l'intersection. 
export interface NombreRationnel
    extends Nombre, Fraction, FabriqueRationnel<NombreRationnel>, Representable, Identifiable<Nombre & Fraction> { }
/*
 * Implementation d'une fabrique avec une classe anonyme.
 */
export function fraction(frac: [number, number]): NombreRationnel {
    return new class implements NombreRationnel {

        valeur(): number {
            return frac[0] / frac[1]
        }

        creerParFraction(f: [number, number]): NombreRationnel {
            return fraction(f);
        }

        creerParNombre(q: number): NombreRationnel {
            return fraction(approximation(q))
        }

        fraction(): [number, number] {
            return frac;
        }

        representation(): string {
            return this.fraction()[0]
                + "/" + this.fraction()[1];
        }

        estEgal(x: NombreRationnel): boolean {
            const f = this.fraction();
            const g = x.fraction();
            return f[0] * g[1] === f[1] * g[0];
        }
    };
};

/*
 * Implémentation d'une fabrique avec une classe anonyme.
 */
export function nombre(val: number): NombreRationnel {
    return new class implements NombreRationnel {

        fraction(): [number, number] {
            return approximation(val)
        }

        creerParFraction(f: [number, number]): NombreRationnel {
            return nombre(f[0] / f[1])
        }

        creerParNombre(q: number): NombreRationnel {
            return nombre(q)
        }

        valeur(): number {
            return val;
        }

        representation(): string {
            return String(this.valeur());
        }

        estEgal(x: NombreRationnel): boolean {
            return this.valeur() === x.valeur();
        }
    };
};
/*
* 2. A FAIRE - La couche haute : agregation avec delegation
*/

abstract class RationnelDeleguantAEtat implements Rationnel {

    constructor(private nombreRationnel: NombreRationnel) {

    }

    fraction(): [number, number] {
        return this.nombreRationnel.fraction();
    }


    valeur(): number {
        return this.nombreRationnel.valeur();
    }

    representation(): string {
        return this.nombreRationnel.representation();
    }

    estEgal(x: RationnelMutable): boolean {
        return this.nombreRationnel.estEgal(x);
    }


    abstract creerParFraction(f: [number, number]): Rationnel;

    abstract creerParNombre(q: number): Rationnel;

    abstract inverse(): Rationnel;

    abstract oppose(): Rationnel;

    abstract produit(x: RationnelMutable): Rationnel;

    abstract somme(x: RationnelMutable): Rationnel;

    abstract un(): Rationnel;

    abstract zero(): Rationnel;

}

interface RationnelOperation {
    somme(x: Rationnel): Rationnel;
    zero(): Rationnel ;
    produit(x: Rationnel): Rationnel;
    un(): Rationnel;
    oppose(): Rationnel;
    inverse(): Rationnel ;
    estEgal(x:Rationnel): Boolean;
    representation(): string ;
}

class RationnelOperationParQuotient extends RationnelDeleguantAEtat implements RationnelOperation {

    constructor(nombreRationnel: NombreRationnel) {
        super(nombreRationnel);
    }

    creerParFraction(f: [number, number]): Rationnel {
        return new RationnelOperationParQuotient(fraction(f));
    }
    creerParNombre(q: number): Rationnel {
        return new RationnelOperationParQuotient(nombre(q));
    }
    somme(x: Rationnel): Rationnel {
        return this.creerParNombre(this.valeur() + x.valeur());
    }
    zero(): Rationnel {
        return this.creerParNombre(0);
    }
    produit(x: Rationnel): Rationnel {
        return this.creerParNombre(this.valeur() * x.valeur());
    }
    un(): Rationnel {
        return this.creerParNombre(1);
    }
    oppose(): Rationnel {
        return this.creerParNombre(-this.valeur());
    }
    inverse(): Rationnel {
        return this.creerParNombre(1 / this.valeur());
    }
    estEgal(x: Rationnel): boolean {
        return this.valeur() === x.valeur();
    }
    representation(): string {
        return String(this.valeur());
    }
}

class RationnelOperationParFraction extends RationnelDeleguantAEtat implements RationnelOperation {

    constructor(nombreRationnel: NombreRationnel) {
        super(nombreRationnel);
    }

    creerParFraction(f: [number, number]): Rationnel {
        return new RationnelOperationParFraction(fraction(f));
    }
    creerParNombre(q: number): Rationnel {
        return new RationnelOperationParFraction(nombre(q));
    }
    somme(x: Rationnel): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[0] * x.fraction()[1] + this.fraction()[1] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);
    }
   
    zero(): Rationnel {
        return this.creerParFraction([0, 1]);
    }
    produit(x: Rationnel): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[0] * x.fraction()[0]
                ,
                this.fraction()[1] * x.fraction()[1]
            ]);

    }
    un(): Rationnel {
        return this.creerParFraction([1, 1]);
    }
    oppose(): Rationnel {
        return this.creerParFraction(
            [
                -this.fraction()[0]
                ,
                this.fraction()[1]
            ]);
    }
    inverse(): Rationnel {
        return this.creerParFraction(
            [
                this.fraction()[1]
                ,
                this.fraction()[0]
            ]);
    }
    estEgal(x: Rationnel): boolean {
        const f = this.fraction();
        const g = x.fraction();
        return f[0] * g[1] === f[1] * g[0];
    }
    representation(): string {
        return this.fraction()[0]
            + "/" + this.fraction()[1];
    }
}



/*
* 3. A Faire - Les patrons etat et Strategie.
*/
/*
* Nombres rationnels enrichis par une interface permettant de 
* determiner la representation et d'obtenir l'autre representation.
*/
interface NombreRationnelDimorphe extends Nombre, Fraction, FabriqueRationnel<NombreRationnelDimorphe>, Representable, Identifiable<Nombre & Fraction> {
    estFraction(): boolean;
    estQuotient(): boolean;
    autreRepresentation(): NombreRationnelDimorphe;
}
/*
 * Implementation d'une fabrique de fractions avec une classe anonyme * par adaptation.
 */
export function fractionDimorphe(frac: [number, number]): NombreRationnelDimorphe {
    return new class implements NombreRationnelDimorphe {
        estFraction(): boolean {
            return true;
        }
        estQuotient(): boolean {
            return false;
        }
        autreRepresentation(): NombreRationnelDimorphe {
            return quotientDimorphe(frac[0] / frac[1]);
        }
        valeur(): number {
            return frac[0] / frac[1];
        }
        fraction(): [number, number] {
            return frac;
        }
        creerParFraction(f: [number, number]): NombreRationnelDimorphe {
            return fractionDimorphe(f);
        }
        creerParNombre(q: number): NombreRationnelDimorphe {
            return fractionDimorphe(approximation(q));
        }
        representation(): string {
            return String(this.valeur());
        }
        estEgal(x: Rationnel): boolean {
            const f = frac;
            const g = x;
            return f[0] * g.fraction()[1] === f[1] *g.fraction()[0]; 
        }

    }
}

export function quotientDimorphe(val: number): NombreRationnelDimorphe {
    return new class implements NombreRationnelDimorphe {
        estFraction(): boolean {
            return true;
        }
        estQuotient(): boolean {
            return false;
        }
        autreRepresentation(): NombreRationnelDimorphe {
            return fractionDimorphe(approximation(val));
        }
        valeur(): number {
            return val;
        }
        fraction(): [number, number] {
            return approximation(val);
        }
        creerParFraction(f: [number, number]): NombreRationnelDimorphe {
            return quotientDimorphe(f[0]/f[1]);
        }
        creerParNombre(q: number): NombreRationnelDimorphe {
            return quotientDimorphe(q);
        }
        representation(): string {
            return String(this.valeur());
        }
        estEgal(x: Rationnel): boolean {
            return val == x.valeur();
        }

    }
}
/*
* Strategie pour les calculs : une fabrique de nombres rationnels  
* dimorphes et un module de corps (voir ModuleCorps de 
* "../structuresAlgebriques/hierarchieModulaire") operant sur les
* nombres rationnels dimorphes.
*/
export interface StrategieCalcul extends   ModuleCorps<NombreRationnelDimorphe>,   FabriqueRationnel<NombreRationnelDimorphe> {

    somme(x: NombreRationnelDimorphe, y: NombreRationnelDimorphe): NombreRationnelDimorphe
    produit(x: NombreRationnelDimorphe, y: NombreRationnelDimorphe): NombreRationnelDimorphe
    zero(): NombreRationnelDimorphe 
    un(): NombreRationnelDimorphe
    oppose(x: NombreRationnelDimorphe): NombreRationnelDimorphe
    inverse(x: NombreRationnelDimorphe): NombreRationnelDimorphe
}
/*
* Rationnels enrichis par une interface d'administration de l'etat 
* (un nombre rationnel dimorphe) et de la strategie de calcul.
*/
export interface RationnelPolymorphe extends Fraction, Nombre, FabriqueRationnel<RationnelPolymorphe>, Corps<RationnelPolymorphe>, Representable, Identifiable<Nombre & Fraction> {
    etat(): NombreRationnelDimorphe;
    estFraction(): boolean;
    estQuotient(): boolean;
    modifierRepresentation(): void;
    modifierStrategie(s: StrategieCalcul): void;
}
// Strategie de calcul par fractions
export const calculsParFraction: StrategieCalcul =
    new class implements StrategieCalcul {
        creerParNombre(q: number): NombreRationnelDimorphe {
            return fractionDimorphe(approximation(q));
        }
        creerParFraction(f: [number,number]): NombreRationnelDimorphe {
            return fractionDimorphe(f);
        }
        somme(x: NombreRationnelDimorphe, y: NombreRationnelDimorphe): NombreRationnelDimorphe {
            return this.creerParFraction(
                [
                    x.fraction()[0] * y.fraction()[1] + x.fraction()[1] * y.fraction()[0]
                    ,
                    x.fraction()[1] * y.fraction()[1]
                ]);
        }
        
        zero(): NombreRationnelDimorphe {
            return this.creerParFraction([0, 1]);
        }
        produit(x: NombreRationnelDimorphe, y: NombreRationnelDimorphe): NombreRationnelDimorphe {
            return this.creerParFraction(
                [
                    x.fraction()[0] * y.fraction()[0]
                    ,
                    x.fraction()[1] * y.fraction()[1]
                ]);
        }
        un(): NombreRationnelDimorphe {
            return this.creerParFraction([1, 1]);
        }
        oppose(x: NombreRationnelDimorphe): NombreRationnelDimorphe {
            return this.creerParFraction(
                [
                    -x.fraction()[0]
                    ,
                    x.fraction()[1]
                ]);
        }
        inverse(x: NombreRationnelDimorphe): NombreRationnelDimorphe {
            return this.creerParFraction(
                [
                    x.fraction()[1]
                    ,
                    x.fraction()[0]
                ]);
        }
    };

    export function monRationnelPolymorphe(etat:NombreRationnelDimorphe, strategie:StrategieCalcul): RationnelPolymorphe {
        return new class implements RationnelPolymorphe {
            etat(): NombreRationnelDimorphe {
                return etat;
            }
            estFraction(): boolean {
                return etat.estFraction();
            }
            estQuotient(): boolean {
                return etat.estQuotient();
            }
            modifierRepresentation(): void {
                etat.autreRepresentation();
            }
            modifierStrategie(s: StrategieCalcul): void {
                strategie = s;
            }
            fraction(): [number, number] {
                return etat.fraction();
            }
            valeur(): number {
                return etat.valeur();   
            }
            creerParFraction(f: [number, number]): RationnelPolymorphe {
                return monRationnelPolymorphe(etat.creerParFraction(f),strategie);   
            }
            creerParNombre(q: number): RationnelPolymorphe {
                return monRationnelPolymorphe(etat.creerParNombre(q),strategie);   
            }
            somme(x: RationnelPolymorphe): RationnelPolymorphe {
                return monRationnelPolymorphe( strategie.somme(x.etat(), etat),strategie);  
            }
            zero(): RationnelPolymorphe {
                return monRationnelPolymorphe( strategie.zero(),strategie);  
            }
            produit(x: RationnelPolymorphe): RationnelPolymorphe {
                return monRationnelPolymorphe( strategie.produit(x.etat(), etat),strategie);  
            }
            un(): RationnelPolymorphe {
                return monRationnelPolymorphe( strategie.un(),strategie);  
            }
            oppose(): RationnelPolymorphe {
                return monRationnelPolymorphe( strategie.oppose(etat),strategie);  
            }
            inverse(): RationnelPolymorphe {
                return monRationnelPolymorphe( strategie.inverse(etat),strategie);  
            }
            representation(): string {
                return etat.representation();
            }
            estEgal(x: Nombre & Fraction): boolean {
                return etat.estEgal(x);
            }
           
    
        }
    }