"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
exports.__esModule = true;
exports.adaptorList = exports.CONS = exports.VIDE = exports.VisiteurPrimitifRecursif = exports.VisiteurRecursif = exports.VisiteurIteratif = void 0;
var listes_1 = require("../bibliotheque/listes");
var listes_2 = require("../bibliotheque/listes");
var listes_3 = require("../bibliotheque/listes");
var listes_4 = require("../bibliotheque/listes");
var listes_5 = require("../bibliotheque/listes");
var listes_6 = require("../bibliotheque/listes");
function VisiteurIteratif() {
    return new /** @class */ (function () {
        function class_1() {
        }
        class_1.prototype.visiterVide = function () {
            return 0;
        };
        class_1.prototype.visiterCons = function (tete, resultatPrecedent) {
            return resultatPrecedent + 1;
        };
        return class_1;
    }());
}
exports.VisiteurIteratif = VisiteurIteratif;
function VisiteurRecursif() {
    return new /** @class */ (function () {
        function class_2() {
        }
        class_2.prototype.visiterVide = function () {
            return 0;
        };
        class_2.prototype.visiterCons = function (tete, resultatPrecedent) {
            return resultatPrecedent + 1;
        };
        return class_2;
    }());
}
exports.VisiteurRecursif = VisiteurRecursif;
function VisiteurPrimitifRecursif() {
    return new /** @class */ (function () {
        function class_3() {
        }
        class_3.prototype.visiterVide = function () {
            return 0;
        };
        class_3.prototype.visiterCons = function (tete, reste, resultatPrecedent) {
            return resultatPrecedent + 1;
        };
        return class_3;
    }());
}
exports.VisiteurPrimitifRecursif = VisiteurPrimitifRecursif;
var ListeIterable = /** @class */ (function () {
    function ListeIterable() {
    }
    ListeIterable.prototype.accueilIteratif = function (v) {
        var e_1, _a;
        var res = v.visiterVide();
        try {
            for (var _b = __values(this), _c = _b.next(); !_c.done; _c = _b.next()) {
                var e = _c.value;
                res = v.visiterCons(e, res);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b["return"])) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return res;
    };
    ListeIterable.prototype[Symbol.iterator] = function () {
        var original = this;
        return new /** @class */ (function () {
            function class_4() {
                this.liste = original;
            }
            class_4.prototype.next = function () {
                if (this.liste.estVide()) {
                    return {
                        value: null,
                        done: true
                    };
                }
                var tete = this.liste.tete();
                this.liste = this.liste.reste();
                return {
                    value: tete,
                    done: false
                };
            };
            return class_4;
        }());
    };
    return ListeIterable;
}());
function VIDE() {
    return new /** @class */ (function (_super) {
        __extends(class_5, _super);
        function class_5() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        class_5.prototype.estVide = function () {
            return true;
        };
        class_5.prototype.tete = function () {
            throw new Error("Erreur : tete() sur liste vide !");
        };
        class_5.prototype.reste = function () {
            throw new Error("Erreur : reste() sur liste vide !");
        };
        class_5.prototype.vide = function () {
            return VIDE();
        };
        class_5.prototype.cons = function (t, r) {
            return CONS(t, r);
        };
        class_5.prototype.taille = function () {
            return 0;
        };
        class_5.prototype.accueilRecursif = function (v) {
            return v.visiterVide();
        };
        class_5.prototype.filtrage = function (casVide, casCons) {
            return casVide();
        };
        class_5.prototype.accueilPrimitifRecursif = function (casVide, casCons) {
            return casVide();
        };
        return class_5;
    }(ListeIterable));
}
exports.VIDE = VIDE;
function CONS(t, r) {
    return new /** @class */ (function (_super) {
        __extends(class_6, _super);
        function class_6() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._taille = 1 + r.taille();
            return _this;
        }
        class_6.prototype.estVide = function () {
            return false;
        };
        class_6.prototype.tete = function () {
            return t;
        };
        class_6.prototype.reste = function () {
            return r;
        };
        class_6.prototype.vide = function () {
            return VIDE();
        };
        class_6.prototype.cons = function (t, r) {
            return CONS(t, r);
        };
        class_6.prototype.taille = function () {
            return this._taille;
        };
        class_6.prototype.accueilRecursif = function (v) {
            return v.visiterCons(this.tete(), this.reste().accueilRecursif(v));
        };
        class_6.prototype.accueilPrimitifRecursif = function (casVide, casCons) {
            return casCons(this.tete(), this.reste());
        };
        class_6.prototype.filtrage = function (casVide, casCons) {
            return casCons(this.tete(), this.reste());
        };
        return class_6;
    }(ListeIterable));
}
exports.CONS = CONS;
function adaptorList(adaptedListe) {
    return new /** @class */ (function (_super) {
        __extends(class_7, _super);
        function class_7() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        class_7.prototype.estVide = function () {
            return listes_1.estVide(adaptedListe);
        };
        class_7.prototype.tete = function () {
            return listes_2.tete(adaptedListe);
        };
        class_7.prototype.reste = function () {
            return adaptorList(listes_3.reste(adaptedListe));
        };
        class_7.prototype.vide = function () {
            return adaptorList(listes_6.VIDE());
        };
        class_7.prototype.cons = function (t, r) {
            var tableau = new Array(t);
            var currentList = r;
            while (!currentList.estVide()) {
                tableau.push(currentList.tete());
            }
            return adaptorList(listes_5.tableauCommeListe(tableau));
        };
        class_7.prototype.taille = function () {
            return listes_4.listeCommeTableau(adaptedListe).length;
        };
        class_7.prototype.accueilRecursif = function (v) {
            if (this.estVide()) {
                return v.visiterVide();
            }
            else {
                return v.visiterCons(this.tete(), this.reste().accueilRecursif(v));
            }
        };
        class_7.prototype.filtrage = function (casVide, casCons) {
            if (this.estVide()) {
                return casVide();
            }
            else {
                return casCons(this.tete(), this.reste());
            }
        };
        class_7.prototype.accueilPrimitifRecursif = function (casVide, casCons) {
            if (this.estVide()) {
                return casVide();
            }
            else {
                return casCons(this.tete(), this.reste());
            }
        };
        return class_7;
    }(ListeIterable));
}
exports.adaptorList = adaptorList;
var fonctionFiltree = function (list, element) {
    console.log(list.tete());
    return list.tete();
};
var list = VIDE();
list = CONS(42, list);
list = CONS(44, list);
list = CONS(57, list);
list = CONS(65, list);
var visiteurIteratif = VisiteurIteratif();
//console.log(list.accueilIteratif<number>(visiteurIteratif));
var visiteurRecursif = VisiteurRecursif();
//console.log(list.accueilRecursif<number>(visiteurRecursif));
var casVide = function () { return 0; };
var casCons = function (t, r) {
    return 1 + r.accueilPrimitifRecursif(casVide, casCons);
};
var visiteurPrimitifRecursif = VisiteurPrimitifRecursif();
//console.log(list.accueilPrimitifRecursif<number>( casVide,casCons));
function taille(liste) {
    return liste.filtrage(// filtrage ou pattern matching
    casVide, casCons);
}
//console.log(f(list));
var adaptatorList = adaptorList(listes_5.tableauCommeListe(new Array(1, 2, 3, 4)));
console.log(adaptatorList.accueilIteratif(visiteurIteratif));
console.log(adaptatorList.accueilRecursif(visiteurRecursif));
console.log(taille(adaptatorList));
