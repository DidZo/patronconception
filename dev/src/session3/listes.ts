import { Liste as TeachersList, listeCommeTableau } from "../bibliotheque/listes";
import { estVide as ESTVIDE } from "../bibliotheque/listes";
import { tete as TETE } from "../bibliotheque/listes";
import { reste as RESTE } from "../bibliotheque/listes";
import { listeCommeTableau as LISTE_VERS_TABLEAU } from "../bibliotheque/listes";
import { tableauCommeListe as TABLEAU_VERS_LISTE } from "../bibliotheque/listes";
import { VIDE as TEACHERVIDE } from "../bibliotheque/listes";


export interface FabriqueListe {
    vide<T>(): Liste<T>;
    cons<T>(t: T, r: Liste<T>): Liste<T>
}

export interface Liste<T> extends FabriqueListe, Iterable<T> {
    estVide(): boolean;
    tete(): T;
    reste(): Liste<T>;
    taille(): number;

    accueilIteratif<R>(v: IVisiteurIteratif<T, R>): R
    accueilRecursif<R>(v: IVisiteurRecursif<T, R>): R
    accueilPrimitifRecursif<R>(
        casVide: () => R,
        casCons: (t: T, r: Liste<T>) => R
    ): R

    filtrage<R>(
        casVide: () => R,
        casCons: (t: T, r: Liste<T>) => R
    ): R
}

export interface IVisiteurIteratif<T,R> {
    visiterVide() : R
    visiterCons(tete: T, resultatPrecedent: R) : R
}

export interface IVisiteurRecursif<T,R> {
    visiterVide() : R
    visiterCons(tete: T, resultatPrecedent: R) : R
}

export interface IVisiteurPrimitifRecursif<T, R> {
    visiterVide() : R
    visiterCons(tete:T, reste: Liste<T>, resultatPrecedent: R) : R
}


export function VisiteurIteratif<T>(): IVisiteurIteratif<T,number> {
    return new class implements IVisiteurIteratif<T, number>{
        visiterVide() : number {
            return 0;
        }
        visiterCons( tete: T, resultatPrecedent: number) : number {
            return  resultatPrecedent+ 1;
        }
    }
}

export function VisiteurRecursif<T>(): IVisiteurRecursif<T,number> {
    return new class implements IVisiteurRecursif<T, number>{
        visiterVide() : number {
            return 0;
        }
        visiterCons( tete: T, resultatPrecedent: number) : number {
            return  resultatPrecedent+ 1;
        }
    }
}

export function VisiteurPrimitifRecursif<T>(): IVisiteurPrimitifRecursif<T,number> {
    return new class implements IVisiteurPrimitifRecursif<T, number>{
        visiterVide(): number {
            return 0;
        }
        visiterCons(tete: T, reste: Liste<T>, resultatPrecedent: number): number {
            return  resultatPrecedent+ 1;
        }
    }
}


abstract class ListeIterable<T>  implements Liste<T>  {
  
    abstract estVide(): boolean;
    abstract tete(): T;
    abstract reste(): Liste<T>;
    abstract vide<E>(): Liste<E>;
    abstract cons<E>(t: E, r: Liste<E>): Liste<E>
    abstract taille() : number;
    abstract accueilRecursif<R>(v: IVisiteurRecursif<T,R>): R
    abstract filtrage<R>(
        casVide: () => R,
        casCons: (t: T, r: Liste<T>) => R
    ): R
    abstract accueilPrimitifRecursif<R>(casVide: () => R, casCons: (t: T, r: Liste<T>) => R): R 
    accueilIteratif<R>(v: IVisiteurIteratif<T,R>): R{
        let res : R = v.visiterVide();
        for(let e of this){
            res = v.visiterCons(e, res);
        }
        return res;
    }

    
    [Symbol.iterator](): Iterator<T, any, undefined> {
        const original: Liste<T> = this;
        return new class implements Iterator<T> {
            private liste: Liste<T> = original;
            next(): IteratorResult<T> {
                if (this.liste.estVide()) {
                    return {
                        value: null,
                        done: true
                    };
                }
                const tete = this.liste.tete();
                this.liste = this.liste.reste();
                return {
                    value: tete,
                    done: false
                };
            }
        };
    }


}

export function VIDE<T>(): Liste<T> {
    return new class extends ListeIterable<T> {
        
        estVide(): boolean {
            return true;
        }
        tete(): T {
            throw new Error("Erreur : tete() sur liste vide !");
        }
        reste(): Liste<T> {
            throw new Error("Erreur : reste() sur liste vide !");
        }
        vide<E>(): Liste<E> {
            return VIDE();
        }
        cons<E>(t: E, r: Liste<E>): Liste<E> {
            return CONS(t, r);
        }

        taille(): number {
            return 0;
        }
        accueilRecursif<R>(v: IVisiteurRecursif<T, R>): R {
            return v.visiterVide();
        }
        filtrage<R>(casVide: () => R, casCons: (t: T, r: Liste<T>) => R): R {
            return casVide();
        }
        accueilPrimitifRecursif<R>(casVide: () => R, casCons: (t: T, r: Liste<T>) => R): R {
            return casVide();
        }
    }
      
}

export function CONS<T>(t: T, r: Liste<T>): Liste<T> {
    return new class extends ListeIterable<T>  {
        estVide(): boolean {
            return false;
        }
        tete(): T {
            return t;
        }
        reste(): Liste<T> {
            return r;
        }
        vide<E>(): Liste<E> {
            return VIDE();
        }
        cons<E>(t: E, r: Liste<E>): Liste<E> {
            return CONS(t, r);
        }
        taille(): number {
            return this._taille;
        }
        accueilRecursif<R>(v: IVisiteurRecursif<T, R>): R {
            return v.visiterCons(this.tete(), this.reste().accueilRecursif(v));
        }
        accueilPrimitifRecursif<R>(casVide: () => R, casCons: (t: T, r: Liste<T>) => R): R {
            return casCons(this.tete(),this.reste());
        }
        
        filtrage<R>(casVide: () => R, casCons: (t: T, r: Liste<T>) => R): R {
            return casCons(this.tete(),this.reste());
        }
        private readonly _taille: number = 1 + r.taille();

    }
       
}



export function adaptorList<T>(adaptedListe: TeachersList<T>): Liste<T> {
    return new class extends ListeIterable<T> implements Liste<T> {
        estVide(): boolean {
            return ESTVIDE(adaptedListe);
        }
        tete(): T {
            return TETE(adaptedListe);
        }
        reste(): Liste<T> {
            return adaptorList(RESTE(adaptedListe));
        }
        vide<E>(): Liste<E> {
            return adaptorList(TEACHERVIDE());
        }
        cons<E>(t: E, r: Liste<E>): Liste<E> {
            let tableau: E[] = new Array(t);
            let currentList = r;
            while(!currentList.estVide()){
                tableau.push(currentList.tete())
            }
            return adaptorList(TABLEAU_VERS_LISTE(tableau));
        }
        taille(): number {
            return LISTE_VERS_TABLEAU(adaptedListe).length;
        }
        accueilRecursif<R>(v: IVisiteurRecursif<T, R>): R {
            if(this.estVide()){
                return v.visiterVide();
            }else{
                return v.visiterCons(this.tete(), this.reste().accueilRecursif(v));
            }
        }
        filtrage<R>(casVide: () => R, casCons: (t: T, r: Liste<T>) => R): R {
            if(this.estVide()){
                return casVide();
            }else{
                return casCons(this.tete(),this.reste());
            }        
        }
        accueilPrimitifRecursif<R>(casVide: () => R, casCons: (t: T, r: Liste<T>) => R): R {
            if(this.estVide()){
                return casVide();
            }else{
                return casCons(this.tete(),this.reste());
            }           }
    }
}

let fonctionFiltree: <T>(list: Liste<T>, element: T)  => T = <T> (list: Liste<T>, element: T) => {
    console.log(list.tete());
    return list.tete()
}; 

let list = VIDE<number>()
list = CONS<number>(42, list)
    list = CONS<number>(44, list)
        list = CONS<number>(57, list)
            list = CONS<number>(65, list)



let visiteurIteratif:IVisiteurIteratif<number,number >= VisiteurIteratif<number>();
//console.log(list.accueilIteratif<number>(visiteurIteratif));

let visiteurRecursif:IVisiteurRecursif<number,number >= VisiteurRecursif<number>();
//console.log(list.accueilRecursif<number>(visiteurRecursif));

let casVide = ():number => 0
let casCons= (t: number, r: Liste<number>):number  => {
    return 1 + r.accueilPrimitifRecursif(casVide, casCons);
}

let visiteurPrimitifRecursif:IVisiteurPrimitifRecursif<number,number >= VisiteurPrimitifRecursif<number>();
//console.log(list.accueilPrimitifRecursif<number>( casVide,casCons));

function taille(liste : Liste<number>) : number {
    return liste.filtrage( // filtrage ou pattern matching
        casVide,
        casCons
    );
}
//console.log(f(list));

let adaptatorList = adaptorList(TABLEAU_VERS_LISTE(new Array(1,2,3,4)))

console.log(adaptatorList.accueilIteratif(visiteurIteratif));
console.log(adaptatorList.accueilRecursif(visiteurRecursif));
console.log(taille(adaptatorList));

