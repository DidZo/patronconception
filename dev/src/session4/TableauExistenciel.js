"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.printTableElements = exports.TableauExistentiel = void 0;
var TableauExistentiel = /** @class */ (function () {
    function TableauExistentiel(tab) {
        this.tab = tab;
    }
    TableauExistentiel.prototype.elimination = function (k) {
        return k(this.tab);
    };
    return TableauExistentiel;
}());
exports.TableauExistentiel = TableauExistentiel;
var fun = function (t) {
    var chaine = "";
    for (var _i = 0, t_1 = t; _i < t_1.length; _i++) {
        var e = t_1[_i];
        chaine += e + "; ";
    }
    return chaine;
};
function printTableElements(tab) {
    return tab.elimination(fun);
}
exports.printTableElements = printTableElements;
var A = /** @class */ (function () {
    function A(nombre) {
        this.nombre = nombre;
    }
    A.prototype.toString = function () {
        return this.nombre.toString();
    };
    return A;
}());
var B = /** @class */ (function (_super) {
    __extends(B, _super);
    function B(nombreP) {
        var _this = _super.call(this, nombreP) || this;
        _this.nombreP = nombreP;
        return _this;
    }
    return B;
}(A));
var C = /** @class */ (function () {
    function C(nombre) {
        this.nombre = nombre;
    }
    return C;
}());
var arrayDeA = new Array();
arrayDeA.push(new A(1));
arrayDeA.push(new A(2));
arrayDeA.push(new A(3));
arrayDeA.push(new A(4));
arrayDeA.push(new A(5));
var tableauExistentiel1 = new TableauExistentiel(arrayDeA);
var arrayDeB = new Array();
arrayDeB.push(new B(1));
arrayDeB.push(new B(2));
arrayDeB.push(new B(3));
arrayDeB.push(new B(4));
arrayDeB.push(new B(5));
var tableauExistentiel2 = new TableauExistentiel(arrayDeB);
console.log(printTableElements(tableauExistentiel2));
