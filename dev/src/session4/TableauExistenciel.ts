export class TableauExistentiel<M> {
    constructor(private tab: Array<M>) {}

    elimination<R>(k: <A extends M>(t: Array<A>) => R): R {
        return k(this.tab);
    }
}

let fun= <A>(t: Array<A>) => {
	let chaine = "";
	for (let e of t){
		chaine += e + "; ";
	}
	return chaine;
}

export function printTableElements<M>(tab : TableauExistentiel<any>){
	return tab.elimination(fun)
}

class Mconcret{
	constructor(private nombre:number){}
	toString():string{
		return this.nombre.toString();
	}
}

class SousM extends Mconcret{
	constructor(private nombreP:number){super(nombreP)}
}

class PasSousM{
	constructor(private nombre:number){}
}

let arrayDeA = new Array<Mconcret>();
arrayDeA.push(new Mconcret(1));
arrayDeA.push(new Mconcret(2));
let tableauExistentiel1 = new TableauExistentiel<Mconcret>(arrayDeA);

let arrayDeB = new Array<SousM>();
arrayDeB.push(new SousM(1));
arrayDeB.push(new SousM(2));
let tableauExistentiel2 = new TableauExistentiel<Mconcret>(arrayDeB);

let arrayDeC = new Array<PasSousM>();
arrayDeC.push(new PasSousM(1));
arrayDeC.push(new PasSousM(2));
let tableauExistentiel3 = new TableauExistentiel<Mconcret>(arrayDeC); //gg

console.log(printTableElements(tableauExistentiel1));
console.log(printTableElements(tableauExistentiel2));
console.log(printTableElements(tableauExistentiel3));