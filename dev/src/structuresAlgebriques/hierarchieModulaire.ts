/*
* Hiérachie de structures algébriques.
*/

export interface ModuleSemiGroupeAdditif<T> {
	somme(x: T, y : T): T;
}

export interface ModuleSemiGroupeMultiplicatif<T> {
	produit(x: T, y : T): T;
}

export interface ModuleUnifereAddition<T> {
	zero(): T;
}

export interface ModuleUnifereMultiplication<T> {
	un(): T;
}

export interface ModuleMonoideAdditif<T>
	extends ModuleSemiGroupeAdditif<T>, ModuleUnifereAddition<T> { }

export interface ModuleMonoideMultiplicatif<T>
	extends ModuleSemiGroupeMultiplicatif<T>, ModuleUnifereMultiplication<T> { }

export interface ModuleSymetriqueAddition<T> {
	oppose(x : T): T;
}

export interface ModuleSymetriqueMultiplication<T> {
	inverse(x : T): T;
}

export interface ModuleGroupeAdditif<T>
	extends ModuleMonoideAdditif<T>, ModuleSymetriqueAddition<T> { }

export interface ModuleGroupeMultiplicatif<T>
	extends ModuleMonoideMultiplicatif<T>, ModuleSymetriqueMultiplication<T> { }

export interface ModuleBiUnifere<T> extends ModuleUnifereAddition<T>, ModuleUnifereMultiplication<T> { }

export interface ModuleBiSymetrique<T>
	extends ModuleSymetriqueAddition<T>, ModuleSymetriqueMultiplication<T> { }

export interface ModuleSemiAnneau<T>
	extends ModuleMonoideAdditif<T>, ModuleSemiGroupeMultiplicatif<T> { }

export interface ModuleSemiAnneauUnitaire<T>
	extends ModuleSemiAnneau<T>, ModuleMonoideMultiplicatif<T>, ModuleBiUnifere<T> { }

export interface ModuleEuclidien<T> {
	modulo(x: T, y : T): T;
	div(x: T, y : T): T;
}

export interface ModuleSemiAnneauUnitaireEuclidien<T>
	extends ModuleSemiAnneau<T>, ModuleMonoideMultiplicatif<T>, ModuleBiUnifere<T>, ModuleEuclidien<T> { }


export interface ModuleAnneau<T> extends ModuleSemiAnneau<T>, ModuleGroupeAdditif<T> { }

export interface ModuleAnneauUnitaireEuclidien<T>
	extends ModuleSemiAnneauUnitaire<T>, ModuleAnneau<T>, ModuleEuclidien<T> { }

export interface ModuleAnneauUnitaire<T> extends ModuleSemiAnneauUnitaire<T>, ModuleAnneau<T> { }

export interface ModuleCorps<T>
	extends ModuleAnneauUnitaire<T>, ModuleGroupeMultiplicatif<T>, ModuleBiSymetrique<T> { }





