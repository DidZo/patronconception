"use strict";
exports.__esModule = true;
exports.expressionOP = exports.cheminDROITE = exports.cheminGAUCHE = exports.cheminVIDE = void 0;
function cheminVIDE() {
    return (new (/** @class */ (function () {
        function class_1() {
        }
        class_1.prototype.filtrage = function (casVide, casGauche, casDroite) {
            return casVide();
        };
        class_1.prototype.toString = function () {
            return "[]";
        };
        return class_1;
    }())));
}
exports.cheminVIDE = cheminVIDE;
function cheminGAUCHE(ch, op, d) {
    return (new (/** @class */ (function () {
        function class_2() {
        }
        class_2.prototype.filtrage = function (casVide, casGauche, casDroite) {
            return casGauche(op, ch, d);
        };
        class_2.prototype.toString = function () {
            return ch.toString() + ".(" + op.toString() + " " + d.toString() + ")";
        };
        return class_2;
    }())));
}
exports.cheminGAUCHE = cheminGAUCHE;
function cheminDROITE(ch, op, g) {
    return (new (/** @class */ (function () {
        function class_3() {
        }
        class_3.prototype.filtrage = function (casVide, casGauche, casDroite) {
            return casGauche(op, ch, g);
        };
        class_3.prototype.toString = function () {
            return ch.toString() + ".(" + op.toString() + " " + g.toString() + ")";
        };
        return class_3;
    }())));
}
exports.cheminDROITE = cheminDROITE;
function expressionOP(f, g, d) {
    return (new (/** @class */ (function () {
        function class_4() {
        }
        class_4.prototype.filtrage = function (casConstante, casOperation) {
            return casOperation(f, g, d);
        };
        /*
         * Notation préfixe. Exemple : 5 + 3 noté (+ 5 3)
         */
        class_4.prototype.toString = function () {
            return "(" + f.toString() + " " + g.toString() + " " + d.toString() + ")";
        };
        return class_4;
    }())));
}
exports.expressionOP = expressionOP;
