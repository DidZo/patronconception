import { Expression } from "./Expression";

export interface Chemin<C, Op> {
    filtrage<R>(
        casVide: () => R,
        casGauche: (op: Op, ch: Chemin<C, Op>, d: Expression<C, Op>) => R,
        casDroite: (op: Op, ch: Chemin<C, Op>, g: Expression<C, Op>) => R
        ): R;

    toString(): string;
}

export function cheminVIDE<C, Op>(): Chemin<C, Op>{
    return (new (class implements Chemin<C, Op>{
        filtrage<R>(
            casVide: () => R,
            casGauche: (op: Op, ch: Chemin<C, Op>, d: Expression<C, Op>) => R,
            casDroite: (op: Op, ch: Chemin<C, Op>, g: Expression<C, Op>) => R)
        : R {
            return casVide();
        }
        toString(): string {
            return "[]";
        }
    }))
}

export function cheminGAUCHE<C, Op>(ch : Chemin<C, Op>, op: Op, d : Expression<C, Op>): Chemin<C, Op>{
    return (new (class implements Chemin<C, Op>{
        filtrage<R>(
            casVide: () => R,
            casGauche: (op: Op, ch: Chemin<C, Op>, d: Expression<C, Op>) => R,
            casDroite: (op: Op, ch: Chemin<C, Op>, g: Expression<C, Op>) => R)
        : R {
            return casGauche(op, ch, d);
        }

        toString(): string {
            return ch.toString() + ".(" + (op as any).toString() + " " + d.toString() + ")";
        }
    }))
}

export function cheminDROITE<C, Op>(ch : Chemin<C, Op>, op: Op, g : Expression<C, Op>): Chemin<C, Op>{
    return (new (class implements Chemin<C, Op>{
        filtrage<R>(
            casVide: () => R,
            casGauche: (op: Op, ch: Chemin<C, Op>, d: Expression<C, Op>) => R,
            casDroite: (op: Op, ch: Chemin<C, Op>, g: Expression<C, Op>) => R)
        : R {
            return casGauche(op, ch, g);
        }

        toString(): string {
            return ch.toString() + ".(" + (op as any).toString() + " " + g.toString() + ")";
        }
    }))
}

export function expressionOP<C, Op>(f: Op, g: Expression<C, Op>, d: Expression<C, Op>)
    : Expression<C, Op> {
    return (new (class implements Expression<C, Op>{

        filtrage<R>(casConstante: (c: C) => R
            , casOperation: (op: Op, g: Expression<C, Op>, d: Expression<C, Op>) => R): R {

            return casOperation(f, g, d);
        }

        /*
         * Notation préfixe. Exemple : 5 + 3 noté (+ 5 3)
         */
        toString(): string {
            return "(" + (f as any).toString() + " " + g.toString() + " " + d.toString() + ")";
        }
    }));
}
