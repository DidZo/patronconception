"use strict";
exports.__esModule = true;
exports.CoupureExpression = void 0;
var Chemin_1 = require("./Chemin");
var Expression_1 = require("./Expression");
var CoupureExpression = /** @class */ (function () {
    function CoupureExpression(ch, exp) {
        this.ch = ch;
        this.exp = exp;
    }
    CoupureExpression.prototype.coupureExpression = function (ch, exp) {
        return new CoupureExpression(ch, exp);
    };
    CoupureExpression.prototype.haut = function () {
        var _this = this;
        return this.ch.filtrage(function () { throw new Error("c'est vide mamene"); }, function (op, ch, d) { return _this.coupureExpression(ch, Expression_1.expressionOP(op, _this.exp, d)); }, function (op, ch, g) { return _this.coupureExpression(ch, Expression_1.expressionOP(op, g, _this.exp)); });
    };
    CoupureExpression.prototype.aCoupureHaut = function () {
        return this.ch.filtrage(function () { return false; }, function (op, ch, d) { return true; }, function (op, ch, g) { return true; });
    };
    CoupureExpression.prototype.gauche = function () {
        var _this = this;
        return this.exp.filtrage(function (c) { throw new Error(); }, function (op, g, d) { return _this.coupureExpression(Chemin_1.cheminGAUCHE(_this.ch, op, d), g); });
    };
    CoupureExpression.prototype.droite = function () {
        var _this = this;
        return this.exp.filtrage(function (c) { throw new Error(); }, function (op, g, d) { return _this.coupureExpression(Chemin_1.cheminDROITE(_this.ch, op, g), d); });
    };
    CoupureExpression.prototype.aCoupuresBas = function () {
        return this.exp.filtrage(function (c) { return false; }, function (op, g, d) { return true; });
    };
    CoupureExpression.prototype.toString = function () {
        return "@" + this.ch.toString() + " : " + this.exp.toString();
    };
    return CoupureExpression;
}());
exports.CoupureExpression = CoupureExpression;
