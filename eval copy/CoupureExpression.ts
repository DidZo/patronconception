import { Chemin, cheminDROITE, cheminGAUCHE } from "./Chemin";
import { Expression, expressionOP } from "./Expression";

export class CoupureExpression<C, Op> {

    public constructor(private ch : Chemin<C, Op> , private exp : Expression<C, Op>){}

    public coupureExpression(ch : Chemin<C, Op> , exp : Expression<C, Op>){
        return new CoupureExpression<C, Op>(ch, exp);
    }

    haut() : CoupureExpression<C, Op> {
        return this.ch.filtrage(
            () => { throw new Error("c'est vide mamene");},
            (op, ch, d) => this.coupureExpression(ch, expressionOP(op, this.exp, d)),
            (op, ch, g) => this.coupureExpression(ch, expressionOP(op, g ,this.exp)),
        );
    }

    aCoupureHaut() : Boolean {
        return this.ch.filtrage(
            () => false,
            (op, ch, d) => true,
            (op, ch, g) => true
        );
    }

    gauche() : CoupureExpression<C, Op> { 
        return this.exp.filtrage(
            (c) => { throw new Error()},
            (op, g, d) => this.coupureExpression(cheminGAUCHE(this.ch,op, d),g)
        );
    }


    droite() : CoupureExpression<C, Op> { 
        return this.exp.filtrage(
            (c) => { throw new Error()},
            (op, g, d) => this.coupureExpression(cheminDROITE(this.ch,op, g),d)
        );
    }

    aCoupuresBas() : Boolean {
        return this.exp.filtrage( 
            (c) => false,
            (op, g, d) => true
        );
    }

    toString() : string {
        return "@"+ this.ch.toString() + " : " + this.exp.toString();
    }
}