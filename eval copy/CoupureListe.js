"use strict";
exports.__esModule = true;
exports.CoupureListe = void 0;
var Liste_1 = require("./Liste");
var Liste_2 = require("./Liste");
var CoupureListe = /** @class */ (function () {
    /*
     * Couple de listes noté ci-dessous (precedents, suivants).
     */
    function CoupureListe(precedents, suivants) {
        this.precedents = precedents;
        this.suivants = suivants;
    }
    /*
    * Fabrique publique.
    */
    CoupureListe.auDebut = function (liste) {
        return new CoupureListe(Liste_2.listeVIDE(), liste); // vide, e1 :: e2 ... en :: vide
    };
    /*
     * Fabrique privée à utiliser dans la suite pour constuire des coupures.
     */
    CoupureListe.prototype.coupure = function (p, s) {
        return new CoupureListe(p, s);
    };
    /*
     * A partir de la coupure (t :: r, s), renvoie la coupure (r, t :: s).
     */
    CoupureListe.prototype.coupurePrecedente = function () {
        var precedentMoinsT = this.precedents.reste();
        var suivantPlusT = Liste_1.listeCONS(this.precedents.tete(), this.suivants);
        return this.coupure(precedentMoinsT, suivantPlusT);
    };
    /*
     * A partir de la coupure (p, t :: r), renvoie la coupure (t :: p, r).
     */
    CoupureListe.prototype.coupureSuivante = function () {
        var precedentPlusT = Liste_1.listeCONS(this.suivants.tete(), this.precedents);
        var suivantMoinsT = this.suivants.reste();
        return this.coupure(precedentPlusT, suivantMoinsT);
    };
    /*
     * A partir de la coupure (t :: r, s), renvoie l'élément t.
     */
    CoupureListe.prototype.valeurPrecedente = function () {
        return this.precedents.tete();
    };
    /*
     * A partir de la coupure (p, t :: r), renvoie l'élément t.
     */
    CoupureListe.prototype.valeurSuivante = function () {
        return this.suivants.tete();
    };
    /*
     * A partir de la coupure (t :: r, s), renvoie la coupure (r, s).
     */
    CoupureListe.prototype.retraitPrecedent = function () {
        return this.coupure(this.precedents.reste(), this.suivants);
    };
    /*
     * A partir de la coupure (p, t :: r), renvoie la coupure (p, r).
     */
    CoupureListe.prototype.retraitSuivant = function () {
        return this.coupure(this.precedents, this.suivants.reste());
    };
    /*
     * A partir de la coupure (p, s), renvoie la coupure (e :: p, s).
     */
    CoupureListe.prototype.ajoutPrecedent = function (e) {
        return this.coupure(Liste_1.listeCONS(e, this.precedents), this.suivants);
    };
    /*
     * A partir de la coupure (p, s), renvoie la coupure (p, e :: s).
     */
    CoupureListe.prototype.ajoutSuivant = function (e) {
        return this.coupure(this.precedents, Liste_1.listeCONS(e, this.suivants));
    };
    /*
     * Teste si valeurPrecedente() est bien défini.
     */
    CoupureListe.prototype.aValeurPrecedente = function () {
        return !this.precedents.estVide();
    };
    /*
     * Teste si valeurSuivante() est bien défini.
     */
    CoupureListe.prototype.aValeurSuivante = function () {
        return !this.suivants.estVide();
    };
    /*
     * Teste si valeurSuivante() est bien défini.
     */
    CoupureListe.prototype.taille = function () {
        return this.precedents.taille() + this.suivants.taille();
    };
    /*
     * Teste si les deux listes sont vides.
     */
    CoupureListe.prototype.estVide = function () {
        return this.precedents.estVide() && this.suivants.estVide();
    };
    return CoupureListe;
}());
exports.CoupureListe = CoupureListe;
