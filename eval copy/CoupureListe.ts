import { listeCONS } from "./Liste";
import { Liste, listeVIDE } from "./Liste";

export class CoupureListe<E> {
    /*
    * Fabrique publique.
    */
    static auDebut<E>(liste: Liste<E>): CoupureListe<E> {
        return new CoupureListe<E>(listeVIDE(), liste); // vide, e1 :: e2 ... en :: vide
    }

    /*
     * Couple de listes noté ci-dessous (precedents, suivants).
     */
    private constructor(private precedents: Liste<E>,
        private suivants: Liste<E>) {
    }

    /*
     * Fabrique privée à utiliser dans la suite pour constuire des coupures.
     */
    private coupure(p: Liste<E>, s: Liste<E>): CoupureListe<E> {
        return new CoupureListe<E>(p, s);
    }

    /*
     * A partir de la coupure (t :: r, s), renvoie la coupure (r, t :: s).
     */
    coupurePrecedente(): CoupureListe<E> {
        let precedentMoinsT = this.precedents.reste();
        let suivantPlusT = listeCONS(this.precedents.tete(), this.suivants);
        return this.coupure(precedentMoinsT,suivantPlusT);
    }

    /*
     * A partir de la coupure (p, t :: r), renvoie la coupure (t :: p, r).
     */
    coupureSuivante(): CoupureListe<E> {
        let precedentPlusT = listeCONS(this.suivants.tete(), this.precedents);
        let suivantMoinsT = this.suivants.reste();
        return this.coupure(precedentPlusT,suivantMoinsT);
    }

    /*
     * A partir de la coupure (t :: r, s), renvoie l'élément t.
     */
    valeurPrecedente(): E {
        return this.precedents.tete();
    }

    /*
     * A partir de la coupure (p, t :: r), renvoie l'élément t.
     */
    valeurSuivante(): E {
        return this.suivants.tete();
    }

    /*
     * A partir de la coupure (t :: r, s), renvoie la coupure (r, s).
     */
    retraitPrecedent(): CoupureListe<E> {
        return this.coupure(this.precedents.reste(),this.suivants);

    }

    /*
     * A partir de la coupure (p, t :: r), renvoie la coupure (p, r).
     */
    retraitSuivant(): CoupureListe<E> {
        return this.coupure(this.precedents,this.suivants.reste());
    }

    /*
     * A partir de la coupure (p, s), renvoie la coupure (e :: p, s).
     */
    ajoutPrecedent(e: E): CoupureListe<E> {
        return this.coupure(listeCONS(e,this.precedents),this.suivants);
    }

    /*
     * A partir de la coupure (p, s), renvoie la coupure (p, e :: s).
     */
    ajoutSuivant(e: E): CoupureListe<E> {
        return this.coupure(this.precedents,listeCONS(e,this.suivants));
    }

    /*
     * Teste si valeurPrecedente() est bien défini.
     */
    aValeurPrecedente(): boolean {
        return !this.precedents.estVide();
    }

    /*
     * Teste si valeurSuivante() est bien défini.
     */
    aValeurSuivante(): boolean {
        return !this.suivants.estVide();
    }

    /*
     * Teste si valeurSuivante() est bien défini.
     */
    taille(): number {
        return this.precedents.taille() + this.suivants.taille();
    }

    /*
     * Teste si les deux listes sont vides.
     */
    estVide(): boolean {
        return this.precedents.estVide() && this.suivants.estVide();
    }

}