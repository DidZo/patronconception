"use strict";
exports.__esModule = true;
exports.expressionOP = exports.expressionCONS = void 0;
function expressionCONS(c) {
    return (new (/** @class */ (function () {
        function class_1() {
        }
        class_1.prototype.filtrage = function (casConstante, casOperation) {
            return casConstante(c);
        };
        class_1.prototype.toString = function () {
            return c.toString();
        };
        return class_1;
    }())));
}
exports.expressionCONS = expressionCONS;
function expressionOP(f, g, d) {
    return (new (/** @class */ (function () {
        function class_2() {
        }
        class_2.prototype.filtrage = function (casConstante, casOperation) {
            return casOperation(f, g, d);
        };
        /*
         * Notation préfixe. Exemple : 5 + 3 noté (+ 5 3)
         */
        class_2.prototype.toString = function () {
            return "(" + f.toString() + " " + g.toString() + " " + d.toString() + ")";
        };
        return class_2;
    }())));
}
exports.expressionOP = expressionOP;
