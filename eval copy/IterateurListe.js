"use strict";
exports.__esModule = true;
exports.IterateurListe = void 0;
var CoupureListe_1 = require("./CoupureListe");
var IterateurListe = /** @class */ (function () {
    function IterateurListe(cl) {
        this.cl = cl;
    }
    IterateurListe.iterateurListe = function (liste) {
        return new IterateurListe(CoupureListe_1.CoupureListe.auDebut(liste));
    };
    IterateurListe.prototype.next = function () {
        if (!this.cl.aValeurSuivante()) {
            return { value: null, done: true };
        }
        else {
            this.cl = this.cl.coupureSuivante();
            return { value: this.cl.valeurSuivante(), done: false };
        }
    };
    return IterateurListe;
}());
exports.IterateurListe = IterateurListe;
