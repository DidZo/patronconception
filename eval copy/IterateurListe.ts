import { Liste } from "./Liste";
import { CoupureListe } from "./CoupureListe";

export class IterateurListe<E> implements Iterator<E> {
    
    private constructor(private cl: CoupureListe<E>) {
    }

    public static iterateurListe<E>(liste: Liste<E>): IterateurListe<E> {
        return new IterateurListe<E>(CoupureListe.auDebut(liste));
    }
    
    next(): IteratorResult<E>{
        if(!this.cl.aValeurSuivante()){
            return { value: null, done: true}
        }else{
            this.cl = this.cl.coupureSuivante();
            return {value: this.cl.valeurSuivante(), done: false}
        }
    }
}