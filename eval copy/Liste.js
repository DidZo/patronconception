"use strict";
exports.__esModule = true;
exports.listeCONS = exports.listeVIDE = exports.listeDEF = void 0;
var IterateurListe_1 = require("./IterateurListe");
function listeDEF() {
    var elts = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        elts[_i] = arguments[_i];
    }
    var res = listeVIDE();
    for (var i = elts.length - 1; i >= 0; i--) {
        res = listeCONS(elts[i], res);
    }
    return res;
}
exports.listeDEF = listeDEF;
function listeVIDE() {
    return (new (/** @class */ (function () {
        function class_1() {
        }
        class_1.prototype[Symbol.iterator] = function () {
            return IterateurListe_1.IterateurListe.iterateurListe(this);
        };
        class_1.prototype.taille = function () {
            return 0;
        };
        class_1.prototype.estVide = function () {
            return true;
        };
        class_1.prototype.tete = function () {
            throw new Error("* Erreur : tete non défini pour une liste vide.");
        };
        class_1.prototype.reste = function () {
            throw new Error("* Erreur : tete non défini pour une liste vide.");
        };
        class_1.prototype.filtrage = function (casVide, casCons) {
            return casVide();
        };
        class_1.prototype.vide = function () {
            return listeVIDE();
        };
        class_1.prototype.cons = function (e) {
            return listeCONS(e, this);
        };
        class_1.prototype.toString = function () {
            return "[]";
        };
        return class_1;
    }())));
}
exports.listeVIDE = listeVIDE;
function listeCONS(tete, reste) {
    return (new (/** @class */ (function () {
        function class_2() {
        }
        class_2.prototype[Symbol.iterator] = function () {
            return IterateurListe_1.IterateurListe.iterateurListe(this);
        };
        class_2.prototype.taille = function () {
            return 1 + reste.taille();
        };
        class_2.prototype.estVide = function () {
            return false;
        };
        class_2.prototype.tete = function () {
            return tete;
        };
        class_2.prototype.reste = function () {
            return reste;
        };
        class_2.prototype.filtrage = function (casVide, casCons) {
            return casCons(tete, reste);
        };
        class_2.prototype.vide = function () {
            return listeVIDE();
        };
        class_2.prototype.cons = function (e) {
            return listeCONS(e, this);
        };
        class_2.prototype.toString = function () {
            return tete.toString() + "::" + reste.toString();
        };
        return class_2;
    }())));
}
exports.listeCONS = listeCONS;
