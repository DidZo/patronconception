"use strict";
exports.__esModule = true;
var Chemin_1 = require("./Chemin");
var CoupureExpression_1 = require("./CoupureExpression");
var Expression_1 = require("./Expression");
var OpArith;
(function (OpArith) {
    OpArith["Plus"] = "+";
    OpArith["Mult"] = "*";
})(OpArith || (OpArith = {}));
var expG = Expression_1.expressionOP(OpArith.Plus, Expression_1.expressionCONS(5), Expression_1.expressionCONS(3));
var expD = Expression_1.expressionOP(OpArith.Plus, Expression_1.expressionCONS(12), Expression_1.expressionCONS(15));
var exp = Expression_1.expressionOP(OpArith.Mult, expG, expD);
console.log("(* (+ 5 3) (+ 12 15)) : " + exp.toString());
console.log("hauteur 3 : " + hauteur(exp));
console.log("hauteur 4 : " + hauteur(Expression_1.expressionOP(OpArith.Mult, exp, Expression_1.expressionCONS(9))));
function hauteur(exp) {
    return exp.filtrage(function (c) { return 1; }, function (op, g, d) { return 1 + Math.max(hauteur(g), hauteur(d)); });
}
var coupure = new CoupureExpression_1.CoupureExpression(Chemin_1.cheminVIDE(), exp);
console.log("[@0 : (* (+ 5 3) (+ 12 15))] : [" + coupure + "]");
coupure = coupure.droite();
console.log("[@0.(* (+ 5 3) ?) : (+ 12 15)] : [" + coupure + "]");
coupure = coupure.gauche();
console.log("[@0.(* (+ 5 3) ?).(+ ? 15) : 12] : [" + coupure + "]");
coupure = coupure.haut();
console.log("[@0.(* (+ 5 3) ?) : (+ 12 15)] : [" + coupure + "]");
