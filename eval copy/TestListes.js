"use strict";
exports.__esModule = true;
var Liste_1 = require("./Liste");
var CoupureListe_1 = require("./CoupureListe");
var l = Liste_1.listeDEF(2, 3, 4, 5, 6, 7);
console.log(l.toString());
console.log("*******");
tester1(CoupureListe_1.CoupureListe.auDebut(l));
console.log("*******");
tester2(l);
console.log("*******");
function tester1(coupure) {
    var res = "";
    while (coupure.aValeurSuivante) {
        res += coupure.valeurSuivante().toString() + " ";
        coupure = coupure.coupureSuivante();
    }
    while (coupure.aValeurPrecedente) {
        res += coupure.valeurPrecedente().toString() + " ";
        coupure = coupure.coupurePrecedente();
    }
    console.log(res);
    console;
}
function tester2(l) {
    for (var _i = 0, l_1 = l; _i < l_1.length; _i++) {
        var e = l_1[_i];
        console.log(e.toString() + " ");
    }
}
