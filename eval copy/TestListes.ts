import { Liste, listeDEF } from "./Liste";
import { CoupureListe } from "./CoupureListe";

const l: Liste<number> = listeDEF(2, 3, 4, 5, 6, 7);
console.log(l.toString());
console.log("*******");
tester1(CoupureListe.auDebut(l));
console.log("*******");
tester2(l);
console.log("*******");

function tester1<E>(coupure: CoupureListe<E>): void {
    let res = "";
    while(coupure.aValeurSuivante){
        res += (coupure.valeurSuivante() as any).toString() + " ";
        coupure = coupure.coupureSuivante();
    }
    while(coupure.aValeurPrecedente){
        res += (coupure.valeurPrecedente() as any).toString() + " ";
        coupure = coupure.coupurePrecedente();
    }
    console.log(res);console
}


function tester2<E>(l: Liste<E>) {
    for(let e of l){
        console.log((e as any).toString() + " " );
    }
}
