import { Chemin, cheminGAUCHE, cheminDROITE } from "./Chemin";
import { Expression, expressionOP } from "./Expression";

export class CoupureExpression<C, Op> {

    constructor(private chemin: Chemin<C, Op>, private expression: Expression<C, Op>) {
    }

    haut(): CoupureExpression<C, Op> {
        return this.chemin.filtrage(() => {
            throw new Error();
        }, (p, f, d) => CoupureExpression.nouveau(p, expressionOP(f, this.expression, d)),
            (p, f, g) => CoupureExpression.nouveau(p, expressionOP(f, g, this.expression)));
    }

    aCoupureHaut(): boolean {
        return this.chemin.filtrage(() => false,
            (p, f, d) => true,
            (p, f, g) => true
        );
    }

    gauche(): CoupureExpression<C, Op> {
        return this.expression.filtrage((c) => {
            throw new Error();
        }, (f, g, d) => CoupureExpression.nouveau(cheminGAUCHE(this.chemin, f, d), g));
    }

    aCoupuresBas(): boolean {
        return this.expression.filtrage((c) => false,
            (f, g, d) => true
        );
    }

    droite(): CoupureExpression<C, Op> {
        return this.expression.filtrage((c) => {
            throw new Error();
        }, (f, g, d) => CoupureExpression.nouveau(cheminDROITE(this.chemin, f, g), d));
    }

    toString(): string {
        return "@" + this.chemin.toString() + " : " + this.expression.toString();
    }
    static nouveau<C, Op>(ch: Chemin<C, Op>, exp: Expression<C, Op>) {
        return new CoupureExpression<C, Op>(ch, exp);
    }
}
