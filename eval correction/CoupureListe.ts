import { Liste, listeVIDE } from "./Liste";

export class CoupureListe<E> {

    static auDebut<E>(liste: Liste<E>): CoupureListe<E> {
        return new CoupureListe<E>(listeVIDE(), liste);
    }

    private constructor(private precedents: Liste<E>,
        private suivants: Liste<E>) {
    }

    private coupure(p: Liste<E>, s: Liste<E>): CoupureListe<E> {
        return new CoupureListe<E>(p, s);
    }

    coupurePrecedente(): CoupureListe<E> {
        return this.coupure(this.precedents.reste(), this.suivants.cons(this.precedents.tete()));
    }

    coupureSuivante(): CoupureListe<E> {
        return this.coupure(this.precedents.cons(this.suivants.tete()), this.suivants.reste());
    }

    valeurPrecedente(): E {
        return this.precedents.tete();
    }

    valeurSuivante(): E {
        return this.suivants.tete();
    }

    retraitPrecedent(): CoupureListe<E> {
        return this.coupure(this.precedents.reste(), this.suivants);
    }

    retraitSuivant(): CoupureListe<E> {
        return this.coupure(this.precedents, this.suivants.reste());

    }

    ajoutPrecedent(e: E): CoupureListe<E> {
        return this.coupure(this.precedents.cons(e), this.suivants);
    }

    ajoutSuivant(e: E): CoupureListe<E> {
        return this.coupure(this.precedents, this.suivants.cons(e));
    }

    aValeurPrecedente(): boolean {
        return !this.precedents.estVide();
    }

    aValeurSuivante(): boolean {
        return !this.suivants.estVide();
    }

    taille(): number {
        return this.precedents.taille() + this.suivants.taille();
    }

    estVide(): boolean {
        return this.taille() == 0;
    }

}