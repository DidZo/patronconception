import { CoupureListe } from "./CoupureListe";
import { Liste } from "./Liste";

export class IterateurListe<E> implements Iterator<E> {
    static nouveau<T>(l: Liste<T>) {
        return new IterateurListe<T>(CoupureListe.auDebut(l));
    }
    private constructor(private coupure: CoupureListe<E>) {
    }
    next(): IteratorResult<E> {
        if (!this.coupure.aValeurSuivante()) {
            return { value: nonDefini(), done: true };
        }
        const t = this.coupure.valeurSuivante();
        this.coupure = this.coupure.coupureSuivante();
        return {
            value: t,
            done: false
        };
    }
}

function nonDefini(): never {
    throw new Error();
}