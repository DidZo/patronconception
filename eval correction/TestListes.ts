import { Liste, listeDEF } from "./Liste";
import { CoupureListe } from "./CoupureListe";

const l: Liste<number> = listeDEF(2, 3, 4, 5, 6, 7);
console.log(l.toString());
tester1(CoupureListe.auDebut(l));
console.log("**");
tester2(l);
console.log("**");
tester3(l);
console.log("**");

function tester1<E>(coupure: CoupureListe<E>): void {
    let affichage = "";
    while (coupure.aValeurSuivante()) {
        affichage = affichage + (coupure.valeurSuivante() as any).toString() + " ";
        coupure = coupure.coupureSuivante();
    }
    while (coupure.aValeurPrecedente()) {
        affichage = affichage + (coupure.valeurPrecedente() as any).toString() + " ";
        coupure = coupure.coupurePrecedente();
    }
    console.log(affichage);
}














function tester2<E>(l: Liste<E>) {
    let affichage = "";
    for (let e of l) {
        affichage = affichage + (e as any).toString() + " ";
    }
    console.log(affichage);
}
function tester3<E>(l: Liste<E>) {
    let affichage = "";
    const iter = l[Symbol.iterator]();
    let suivant = iter.next();
    while (!suivant.done) {
        const e = suivant.value;
        affichage = affichage + (e as any).toString() + " ";
        suivant = iter.next();
    }
    console.log("iter : " + affichage);
}
