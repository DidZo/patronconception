import { Expression } from "./Expression";

export interface Chemin<C, Op> {
    filtrage<R>(casVide: () => R,
        casGauche: (ch: Chemin<C, Op>, f: Op, d: Expression<C, Op>) => R,
        casDroite: (ch: Chemin<C, Op>, f: Op, g: Expression<C, Op>) => R): R;
    toString(): string;
}

export function cheminVIDE<C, Op>(): Chemin<C, Op> {
    return (new (class implements Chemin<C, Op> {
        filtrage<R>(casVide: () => R,
            casGauche: (ch: Chemin<C, Op>, f: Op, d: Expression<C, Op>) => R,
            casDroite: (ch: Chemin<C, Op>, f: Op, g: Expression<C, Op>) => R): R {
            return casVide();
        }

        toString(): string {
            return "0";
        }
    }));
}

export function cheminGAUCHE<C, Op>(parent: Chemin<C, Op>,
    f: Op, expDroite: Expression<C, Op>): Chemin<C, Op> {
    return (new (class implements Chemin<C, Op> {
        filtrage<R>(casVide: () => R,
            casGauche: (ch: Chemin<C, Op>, f: Op, d: Expression<C, Op>) => R,
            casDroite: (ch: Chemin<C, Op>, f: Op, g: Expression<C, Op>) => R): R {
            return casGauche(parent, f, expDroite);
        }

        toString(): string {
            return parent.toString() + ".(" + (f as any).toString() + " ? " + expDroite.toString() + ")";
        }
    }));
}

export function cheminDROITE<C, Op>(parent: Chemin<C, Op>,
    f: Op, expGauche: Expression<C, Op>): Chemin<C, Op> {
    return (new (class implements Chemin<C, Op> {
        filtrage<R>(casVide: () => R,
            casGauche: (ch: Chemin<C, Op>, f: Op, d: Expression<C, Op>) => R,
            casDroite: (ch: Chemin<C, Op>, f: Op, g: Expression<C, Op>) => R): R {
            return casDroite(parent, f, expGauche);
        }

        toString(): string {
            return parent.toString() + ".(" + (f as any).toString() + " " + expGauche.toString() + " ?)";
        }
    }));
}
