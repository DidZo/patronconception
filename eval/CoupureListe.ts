import { Liste, listeVIDE } from "./Liste";

export class CoupureListe<E> {
    /*
    * Fabrique publique.
    */
    static auDebut<E>(liste: Liste<E>): CoupureListe<E> {
        return new CoupureListe<E>(listeVIDE(), liste);
    }

    /*
     * Couple de listes noté ci-dessous (precedents, suivants).
     */
    private constructor(private precedents: Liste<E>,
        private suivants: Liste<E>) {
    }

    /*
     * Fabrique privée à utiliser dans la suite pour constuire des coupures.
     */
    private coupure(p: Liste<E>, s: Liste<E>): CoupureListe<E> {
        return new CoupureListe<E>(p, s);
    }

    /*
     * A partir de la coupure (t :: r, s), renvoie la coupure (r, t :: s).
     */
    coupurePrecedente(): CoupureListe<E> {
        throw new Error(); // TODO
    }

    /*
     * A partir de la coupure (p, t :: r), renvoie la coupure (t :: p, r).
     */
    coupureSuivante(): CoupureListe<E> {
        throw new Error(); // TODO
    }

    /*
     * A partir de la coupure (t :: r, s), renvoie l'élément t.
     */
    valeurPrecedente(): E {
        throw new Error(); // TODO
    }

    /*
     * A partir de la coupure (p, t :: r), renvoie l'élément t.
     */
    valeurSuivante(): E {
        throw new Error(); // TODO
    }

    /*
     * A partir de la coupure (t :: r, s), renvoie la coupure (r, s).
     */
    retraitPrecedent(): CoupureListe<E> {
        throw new Error(); // TODO
    }

    /*
     * A partir de la coupure (p, t :: r), renvoie la coupure (p, r).
     */
    retraitSuivant(): CoupureListe<E> {
        throw new Error(); // TODO

    }

    /*
     * A partir de la coupure (p, s), renvoie la coupure (e :: p, s).
     */
    ajoutPrecedent(e: E): CoupureListe<E> {
        throw new Error(); // TODO
    }

    /*
     * A partir de la coupure (p, s), renvoie la coupure (p, e :: s).
     */
    ajoutSuivant(e: E): CoupureListe<E> {
        throw new Error(); // TODO
    }

    /*
     * Teste si valeurPrecedente() est bien défini.
     */
    aValeurPrecedente(): boolean {
        throw new Error(); // TODO
    }

    /*
     * Teste si valeurSuivante() est bien défini.
     */
    aValeurSuivante(): boolean {
        throw new Error(); // TODO
    }

    /*
     * Teste si valeurSuivante() est bien défini.
     */
    taille(): number {
        throw new Error(); // TODO
    }

    /*
     * Teste si les deux listes sont vides.
     */
    estVide(): boolean {
        throw new Error(); // TODO
    }

}